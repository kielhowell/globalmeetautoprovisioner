﻿namespace GlobalMeetAutoProvisionerConfig
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_APIId = new System.Windows.Forms.TextBox();
            this.lbl_APIId = new System.Windows.Forms.Label();
            this.lbl_APIPassword = new System.Windows.Forms.Label();
            this.txt_APIPassword = new System.Windows.Forms.TextBox();
            this.lbl_O365Password = new System.Windows.Forms.Label();
            this.txt_O365Password = new System.Windows.Forms.TextBox();
            this.lbl_O365Username = new System.Windows.Forms.Label();
            this.txt_O365Username = new System.Windows.Forms.TextBox();
            this.lbl_CompanyId = new System.Windows.Forms.Label();
            this.txt_CompanyId = new System.Windows.Forms.TextBox();
            this.btn_BrowseFolder = new System.Windows.Forms.Button();
            this.logfileFolderBrosweDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.lbl_LogFilePathResult = new System.Windows.Forms.Label();
            this.btn_SaveForm = new System.Windows.Forms.Button();
            this.btn_Exit = new System.Windows.Forms.Button();
            this.lbl_DefaultLanguage = new System.Windows.Forms.Label();
            this.cbx_DefaultLanguage = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbx_Env = new System.Windows.Forms.ComboBox();
            this.lbl_Env = new System.Windows.Forms.Label();
            this.ckbx_Create = new System.Windows.Forms.CheckBox();
            this.ckbx_Delete = new System.Windows.Forms.CheckBox();
            this.btn_Run = new System.Windows.Forms.Button();
            this.btn_ViewLogs = new System.Windows.Forms.Button();
            this.pBar_RunProgress = new System.Windows.Forms.ProgressBar();
            this.ckbx_DepartmentlBilling = new System.Windows.Forms.CheckBox();
            this.btn_Filters = new System.Windows.Forms.Button();
            this.lbl_SaveFilters = new System.Windows.Forms.Label();
            this.lbl_SaveFieldMappings = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_APIId
            // 
            this.txt_APIId.Location = new System.Drawing.Point(210, 88);
            this.txt_APIId.Name = "txt_APIId";
            this.txt_APIId.Size = new System.Drawing.Size(274, 22);
            this.txt_APIId.TabIndex = 3;
            // 
            // lbl_APIId
            // 
            this.lbl_APIId.AutoSize = true;
            this.lbl_APIId.Location = new System.Drawing.Point(16, 91);
            this.lbl_APIId.Name = "lbl_APIId";
            this.lbl_APIId.Size = new System.Drawing.Size(46, 17);
            this.lbl_APIId.TabIndex = 1;
            this.lbl_APIId.Text = "API ID";
            // 
            // lbl_APIPassword
            // 
            this.lbl_APIPassword.AutoSize = true;
            this.lbl_APIPassword.Location = new System.Drawing.Point(16, 116);
            this.lbl_APIPassword.Name = "lbl_APIPassword";
            this.lbl_APIPassword.Size = new System.Drawing.Size(94, 17);
            this.lbl_APIPassword.TabIndex = 3;
            this.lbl_APIPassword.Text = "API Password";
            // 
            // txt_APIPassword
            // 
            this.txt_APIPassword.Location = new System.Drawing.Point(210, 113);
            this.txt_APIPassword.Name = "txt_APIPassword";
            this.txt_APIPassword.Size = new System.Drawing.Size(274, 22);
            this.txt_APIPassword.TabIndex = 4;
            // 
            // lbl_O365Password
            // 
            this.lbl_O365Password.AutoSize = true;
            this.lbl_O365Password.Location = new System.Drawing.Point(16, 66);
            this.lbl_O365Password.Name = "lbl_O365Password";
            this.lbl_O365Password.Size = new System.Drawing.Size(138, 17);
            this.lbl_O365Password.TabIndex = 15;
            this.lbl_O365Password.Text = "Office 365 Password";
            // 
            // txt_O365Password
            // 
            this.txt_O365Password.Location = new System.Drawing.Point(210, 63);
            this.txt_O365Password.Name = "txt_O365Password";
            this.txt_O365Password.Size = new System.Drawing.Size(274, 22);
            this.txt_O365Password.TabIndex = 2;
            // 
            // lbl_O365Username
            // 
            this.lbl_O365Username.AutoSize = true;
            this.lbl_O365Username.Location = new System.Drawing.Point(15, 42);
            this.lbl_O365Username.Name = "lbl_O365Username";
            this.lbl_O365Username.Size = new System.Drawing.Size(142, 17);
            this.lbl_O365Username.TabIndex = 13;
            this.lbl_O365Username.Text = "Office 365 Username";
            // 
            // txt_O365Username
            // 
            this.txt_O365Username.Location = new System.Drawing.Point(209, 39);
            this.txt_O365Username.Name = "txt_O365Username";
            this.txt_O365Username.Size = new System.Drawing.Size(274, 22);
            this.txt_O365Username.TabIndex = 1;
            // 
            // lbl_CompanyId
            // 
            this.lbl_CompanyId.AutoSize = true;
            this.lbl_CompanyId.Location = new System.Drawing.Point(15, 18);
            this.lbl_CompanyId.Name = "lbl_CompanyId";
            this.lbl_CompanyId.Size = new System.Drawing.Size(84, 17);
            this.lbl_CompanyId.TabIndex = 9;
            this.lbl_CompanyId.Text = "Company ID";
            // 
            // txt_CompanyId
            // 
            this.txt_CompanyId.Location = new System.Drawing.Point(209, 15);
            this.txt_CompanyId.Name = "txt_CompanyId";
            this.txt_CompanyId.Size = new System.Drawing.Size(274, 22);
            this.txt_CompanyId.TabIndex = 0;
            this.txt_CompanyId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_CompanyId_KeyPress);
            // 
            // btn_BrowseFolder
            // 
            this.btn_BrowseFolder.Location = new System.Drawing.Point(18, 286);
            this.btn_BrowseFolder.Name = "btn_BrowseFolder";
            this.btn_BrowseFolder.Size = new System.Drawing.Size(124, 32);
            this.btn_BrowseFolder.TabIndex = 11;
            this.btn_BrowseFolder.Text = "Log File Path";
            this.btn_BrowseFolder.UseVisualStyleBackColor = true;
            this.btn_BrowseFolder.Click += new System.EventHandler(this.btn_BrowseFolder_Click);
            // 
            // lbl_LogFilePathResult
            // 
            this.lbl_LogFilePathResult.AutoSize = true;
            this.lbl_LogFilePathResult.Location = new System.Drawing.Point(146, 294);
            this.lbl_LogFilePathResult.Name = "lbl_LogFilePathResult";
            this.lbl_LogFilePathResult.Size = new System.Drawing.Size(0, 17);
            this.lbl_LogFilePathResult.TabIndex = 17;
            // 
            // btn_SaveForm
            // 
            this.btn_SaveForm.Location = new System.Drawing.Point(18, 373);
            this.btn_SaveForm.Name = "btn_SaveForm";
            this.btn_SaveForm.Size = new System.Drawing.Size(124, 32);
            this.btn_SaveForm.TabIndex = 13;
            this.btn_SaveForm.Text = "Save";
            this.btn_SaveForm.UseVisualStyleBackColor = true;
            this.btn_SaveForm.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Exit
            // 
            this.btn_Exit.Location = new System.Drawing.Point(365, 373);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(124, 32);
            this.btn_Exit.TabIndex = 15;
            this.btn_Exit.Text = "Exit";
            this.btn_Exit.UseVisualStyleBackColor = true;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // lbl_DefaultLanguage
            // 
            this.lbl_DefaultLanguage.AutoSize = true;
            this.lbl_DefaultLanguage.Location = new System.Drawing.Point(17, 142);
            this.lbl_DefaultLanguage.Name = "lbl_DefaultLanguage";
            this.lbl_DefaultLanguage.Size = new System.Drawing.Size(121, 17);
            this.lbl_DefaultLanguage.TabIndex = 19;
            this.lbl_DefaultLanguage.Text = "Default Language";
            // 
            // cbx_DefaultLanguage
            // 
            this.cbx_DefaultLanguage.FormattingEnabled = true;
            this.cbx_DefaultLanguage.Items.AddRange(new object[] {
            "- Select One -",
            "English",
            "French",
            "German",
            "Japanese"});
            this.cbx_DefaultLanguage.Location = new System.Drawing.Point(211, 139);
            this.cbx_DefaultLanguage.Name = "cbx_DefaultLanguage";
            this.cbx_DefaultLanguage.Size = new System.Drawing.Size(274, 24);
            this.cbx_DefaultLanguage.TabIndex = 5;
            this.cbx_DefaultLanguage.SelectionChangeCommitted += new System.EventHandler(this.cbx_DefaultLanguage_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 429);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(367, 17);
            this.label1.TabIndex = 21;
            this.label1.Text = "© 2017 Premiere Global Services, Inc. and/or its affiliates";
            // 
            // cbx_Env
            // 
            this.cbx_Env.FormattingEnabled = true;
            this.cbx_Env.Items.AddRange(new object[] {
            "- Select One -",
            "QAB",
            "PROD"});
            this.cbx_Env.Location = new System.Drawing.Point(211, 166);
            this.cbx_Env.Name = "cbx_Env";
            this.cbx_Env.Size = new System.Drawing.Size(274, 24);
            this.cbx_Env.TabIndex = 6;
            // 
            // lbl_Env
            // 
            this.lbl_Env.AutoSize = true;
            this.lbl_Env.Location = new System.Drawing.Point(17, 169);
            this.lbl_Env.Name = "lbl_Env";
            this.lbl_Env.Size = new System.Drawing.Size(87, 17);
            this.lbl_Env.TabIndex = 22;
            this.lbl_Env.Text = "Environment";
            // 
            // ckbx_Create
            // 
            this.ckbx_Create.AutoSize = true;
            this.ckbx_Create.Location = new System.Drawing.Point(20, 204);
            this.ckbx_Create.Name = "ckbx_Create";
            this.ckbx_Create.Size = new System.Drawing.Size(113, 21);
            this.ckbx_Create.TabIndex = 7;
            this.ckbx_Create.Text = "Create Users";
            this.ckbx_Create.UseVisualStyleBackColor = true;
            // 
            // ckbx_Delete
            // 
            this.ckbx_Delete.AutoSize = true;
            this.ckbx_Delete.Location = new System.Drawing.Point(173, 204);
            this.ckbx_Delete.Name = "ckbx_Delete";
            this.ckbx_Delete.Size = new System.Drawing.Size(112, 21);
            this.ckbx_Delete.TabIndex = 8;
            this.ckbx_Delete.Text = "Delete Users";
            this.ckbx_Delete.UseVisualStyleBackColor = true;
            // 
            // btn_Run
            // 
            this.btn_Run.Location = new System.Drawing.Point(193, 373);
            this.btn_Run.Name = "btn_Run";
            this.btn_Run.Size = new System.Drawing.Size(124, 32);
            this.btn_Run.TabIndex = 14;
            this.btn_Run.Text = "Run";
            this.btn_Run.UseVisualStyleBackColor = true;
            this.btn_Run.Click += new System.EventHandler(this.btn_Run_Click);
            // 
            // btn_ViewLogs
            // 
            this.btn_ViewLogs.Location = new System.Drawing.Point(18, 330);
            this.btn_ViewLogs.Name = "btn_ViewLogs";
            this.btn_ViewLogs.Size = new System.Drawing.Size(124, 32);
            this.btn_ViewLogs.TabIndex = 12;
            this.btn_ViewLogs.Text = "View Logs";
            this.btn_ViewLogs.UseVisualStyleBackColor = true;
            this.btn_ViewLogs.Click += new System.EventHandler(this.btn_ViewLogs_Click);
            // 
            // pBar_RunProgress
            // 
            this.pBar_RunProgress.Location = new System.Drawing.Point(149, 330);
            this.pBar_RunProgress.Name = "pBar_RunProgress";
            this.pBar_RunProgress.Size = new System.Drawing.Size(340, 32);
            this.pBar_RunProgress.TabIndex = 23;
            this.pBar_RunProgress.Visible = false;
            // 
            // ckbx_DepartmentlBilling
            // 
            this.ckbx_DepartmentlBilling.AutoSize = true;
            this.ckbx_DepartmentlBilling.Location = new System.Drawing.Point(328, 204);
            this.ckbx_DepartmentlBilling.Name = "ckbx_DepartmentlBilling";
            this.ckbx_DepartmentlBilling.Size = new System.Drawing.Size(156, 21);
            this.ckbx_DepartmentlBilling.TabIndex = 9;
            this.ckbx_DepartmentlBilling.Text = "Departmental Billing";
            this.ckbx_DepartmentlBilling.UseVisualStyleBackColor = true;
            // 
            // btn_Filters
            // 
            this.btn_Filters.Location = new System.Drawing.Point(18, 248);
            this.btn_Filters.Name = "btn_Filters";
            this.btn_Filters.Size = new System.Drawing.Size(124, 32);
            this.btn_Filters.TabIndex = 10;
            this.btn_Filters.Text = "Filters";
            this.btn_Filters.UseVisualStyleBackColor = true;
            this.btn_Filters.Click += new System.EventHandler(this.btn_Filters_Click);
            // 
            // lbl_SaveFilters
            // 
            this.lbl_SaveFilters.AutoSize = true;
            this.lbl_SaveFilters.Location = new System.Drawing.Point(460, 613);
            this.lbl_SaveFilters.Name = "lbl_SaveFilters";
            this.lbl_SaveFilters.Size = new System.Drawing.Size(0, 17);
            this.lbl_SaveFilters.TabIndex = 25;
            this.lbl_SaveFilters.Visible = false;
            // 
            // lbl_SaveFieldMappings
            // 
            this.lbl_SaveFieldMappings.AutoSize = true;
            this.lbl_SaveFieldMappings.Location = new System.Drawing.Point(460, 401);
            this.lbl_SaveFieldMappings.Name = "lbl_SaveFieldMappings";
            this.lbl_SaveFieldMappings.Size = new System.Drawing.Size(0, 17);
            this.lbl_SaveFieldMappings.TabIndex = 26;
            this.lbl_SaveFieldMappings.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(505, 497);
            this.Controls.Add(this.lbl_SaveFieldMappings);
            this.Controls.Add(this.lbl_SaveFilters);
            this.Controls.Add(this.btn_Filters);
            this.Controls.Add(this.ckbx_DepartmentlBilling);
            this.Controls.Add(this.pBar_RunProgress);
            this.Controls.Add(this.btn_ViewLogs);
            this.Controls.Add(this.btn_Run);
            this.Controls.Add(this.ckbx_Delete);
            this.Controls.Add(this.ckbx_Create);
            this.Controls.Add(this.cbx_Env);
            this.Controls.Add(this.lbl_Env);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbx_DefaultLanguage);
            this.Controls.Add(this.lbl_DefaultLanguage);
            this.Controls.Add(this.btn_Exit);
            this.Controls.Add(this.btn_SaveForm);
            this.Controls.Add(this.lbl_LogFilePathResult);
            this.Controls.Add(this.btn_BrowseFolder);
            this.Controls.Add(this.lbl_O365Password);
            this.Controls.Add(this.txt_O365Password);
            this.Controls.Add(this.lbl_O365Username);
            this.Controls.Add(this.txt_O365Username);
            this.Controls.Add(this.lbl_CompanyId);
            this.Controls.Add(this.txt_CompanyId);
            this.Controls.Add(this.lbl_APIPassword);
            this.Controls.Add(this.txt_APIPassword);
            this.Controls.Add(this.lbl_APIId);
            this.Controls.Add(this.txt_APIId);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "GlobalMeet Auto Provisioner Config";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_APIId;
        private System.Windows.Forms.Label lbl_APIId;
        private System.Windows.Forms.Label lbl_APIPassword;
        private System.Windows.Forms.TextBox txt_APIPassword;
        private System.Windows.Forms.Label lbl_O365Password;
        private System.Windows.Forms.TextBox txt_O365Password;
        private System.Windows.Forms.Label lbl_O365Username;
        private System.Windows.Forms.TextBox txt_O365Username;
        private System.Windows.Forms.Label lbl_CompanyId;
        private System.Windows.Forms.TextBox txt_CompanyId;
        private System.Windows.Forms.Button btn_BrowseFolder;
        private System.Windows.Forms.FolderBrowserDialog logfileFolderBrosweDialog;
        private System.Windows.Forms.Label lbl_LogFilePathResult;
        private System.Windows.Forms.Button btn_SaveForm;
        private System.Windows.Forms.Button btn_Exit;
        private System.Windows.Forms.Label lbl_DefaultLanguage;
        private System.Windows.Forms.ComboBox cbx_DefaultLanguage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbx_Env;
        private System.Windows.Forms.Label lbl_Env;
        private System.Windows.Forms.CheckBox ckbx_Create;
        private System.Windows.Forms.CheckBox ckbx_Delete;
        private System.Windows.Forms.Button btn_Run;
        private System.Windows.Forms.Button btn_ViewLogs;
        private System.Windows.Forms.ProgressBar pBar_RunProgress;
        private System.Windows.Forms.CheckBox ckbx_DepartmentlBilling;
        private System.Windows.Forms.Button btn_Filters;
        private System.Windows.Forms.Label lbl_SaveFilters;
        private System.Windows.Forms.Label lbl_SaveFieldMappings;
    }
}

