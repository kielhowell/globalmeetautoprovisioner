﻿using System;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Resources;
using System.Globalization;
using System.Diagnostics;
using System.Threading;

namespace GlobalMeetAutoProvisionerConfig
{
    public partial class Form1 : Form
    {
        private string _env = "qab";
        private string _webId = "";
        private string _webPw = "";
        private int _pid = 0;

        BackgroundWorker _bgw = new BackgroundWorker();

        public Form1()
        {
            InitializeComponent();
        }

        private void BGW_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ProcessStartInfo _si = new ProcessStartInfo();
                string _filePath = Application.StartupPath.Replace("AutoProvisionerConfig", "GlobalMeetAutoProvisioner") + "\\GlobalMeetAutoProvisioner.exe";
                //MessageBox.Show(_filePath);
                //this.Close();
                _si.FileName = _filePath;
                _si.UseShellExecute = false;
                _si.CreateNoWindow = true;
                _si.WindowStyle = ProcessWindowStyle.Hidden;
                //_si.RedirectStandardOutput = true;

                foreach (Control ctrl in Form.ActiveForm.Controls)
                {
                    if(ctrl.Name == "pBar_RunProgress")
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            pBar_RunProgress.Visible = true;
                            pBar_RunProgress.Style = ProgressBarStyle.Marquee;
                            pBar_RunProgress.MarqueeAnimationSpeed = 30;
                        }));
                    }
                }

                try
                {
                    Process _winProc = Process.Start(_si);
                    _pid = _winProc.Id;
                    _winProc.EnableRaisingEvents = true;
                    _winProc.Exited += new EventHandler(Process_Exited);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btn_BrowseFolder_Click(object sender, EventArgs e)
        {
            if(logfileFolderBrosweDialog.ShowDialog() == DialogResult.OK)
            {
                lbl_LogFilePathResult.Text = logfileFolderBrosweDialog.SelectedPath;
            }
        }

        private void txt_CompanyId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }            
        }

        private void txt_APIId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Run_Click(object sender, EventArgs e)
        {
            btn_SaveForm.PerformClick();
            _bgw.RunWorkerAsync();
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            try
            {
                string _pass = "";
                string _filterStart = " -filter {AcpInfo -eq $null";
                string _deleteFilter = "|Where {";
                List<string> _fieldMappings = new List<string>();
                string _fieldMappingsFinal = "";
                const string _filterEnd = "} ";

                using (PowerShell ps = CreatePowershell())
                {
                    ps.AddScript(string.Format("$pw=\"{0}\" | ConvertTo-SecureString -AsPlainText -Force| convertfrom-securestring;echo $pw", CheckForSpecialChars(txt_O365Password.Text))); //| out-file \"{1}\" -Force   , _passwordFilePathName 
                    ps.Invoke();

                    foreach (var _psItem in ps.Invoke())
                    {
                        if (_psItem != null)
                        {
                            _pass = _psItem.ToString();
                        }
                    }
                    ps.Runspace.Close();
                    ps.Runspace.Dispose();
                }

                if (!System.IO.Directory.Exists(lbl_LogFilePathResult.Text + "\\config"))
                {
                    System.IO.Directory.CreateDirectory(lbl_LogFilePathResult.Text + "\\config");
                    System.IO.File.Create(lbl_LogFilePathResult.Text + "\\config\\config.txt").Close();
                }
                if (!System.IO.File.Exists(lbl_LogFilePathResult.Text + "\\config\\config.txt"))
                {
                    System.IO.File.Create(lbl_LogFilePathResult.Text + "\\config\\config.txt").Close();
                }

                //Save values in Form2
                foreach(Form _frm in Application.OpenForms)
                {
                    if(_frm.GetType() == typeof(Form2))
                    {
                        foreach (GroupBox _gbpx in _frm.Controls.OfType<GroupBox>())
                        {
                            foreach (Panel _pnl in _gbpx.Controls.OfType<Panel>())
                            {
                                if (_pnl.Name == "pnl_Filters")
                                {
                                    string _operator = "";

                                    foreach (Control _ctrl in _pnl.Controls)
                                    {
                                        //figure out how to do multiples
                                        if(ckbx_Delete.Checked)
                                        {
                                            if (_ctrl.GetType() == typeof(ComboBox) && _ctrl.Name.Contains("AndOr"))
                                            {
                                                if (((ComboBox)_ctrl).SelectedItem.ToString().Trim() != "")
                                                {
                                                    _deleteFilter += " -" + ((ComboBox)_ctrl).SelectedItem.ToString() + " ";
                                                }
                                            }
                                            else if (_ctrl.GetType() == typeof(ComboBox) && ((ComboBox)_ctrl).SelectedIndex > 0 && !_ctrl.Name.Contains("Operator") && !_ctrl.Name.Contains("AndOr"))
                                            {
                                                _deleteFilter += "($_." + ((ComboBox)_ctrl).SelectedItem.ToString().Replace(" ","");
                                            }
                                            else if (_ctrl.GetType() == typeof(ComboBox) && ((ComboBox)_ctrl).SelectedIndex > 0 && _ctrl.Name.Contains("Operator"))
                                            {
                                                _operator = ((ComboBox)_ctrl).SelectedItem.ToString();
                                                switch (((ComboBox)_ctrl).SelectedItem.ToString())
                                                {
                                                    case "Equals":
                                                        _deleteFilter += " -eq ";
                                                        break;
                                                    case "Not Equal":
                                                        _deleteFilter += " -ne ";
                                                        break;
                                                    case "Contains":
                                                        _deleteFilter += " -like ";
                                                        break;
                                                }

                                            }
                                            else if (_ctrl.GetType() == typeof(TextBox) && !string.IsNullOrWhiteSpace(_ctrl.Text))
                                            {
                                                _deleteFilter += _operator == "Contains" ? "'*" + _ctrl.Text + "*') " : "\"" + _ctrl.Text + "\")";
                                            }
                                        }

                                        if (_ctrl.GetType() == typeof(ComboBox) && _ctrl.Name.Contains("AndOr"))
                                        {
                                            if (((ComboBox)_ctrl).SelectedItem.ToString().Trim() != "")
                                            {
                                                _filterStart += " -" + ((ComboBox)_ctrl).SelectedItem.ToString() + " ";
                                            }
                                            else
                                            {
                                                _filterStart += " -And ";
                                            }
                                        }
                                        else if (_ctrl.GetType() == typeof(ComboBox) && ((ComboBox)_ctrl).SelectedIndex > 0 && !_ctrl.Name.Contains("Operator") && !_ctrl.Name.Contains("AndOr"))
                                        {
                                            _filterStart += ((ComboBox)_ctrl).SelectedItem.ToString().Replace(" ","");
                                        }
                                        else if (_ctrl.GetType() == typeof(ComboBox) && ((ComboBox)_ctrl).SelectedIndex > 0 && _ctrl.Name.Contains("Operator"))
                                        {
                                            _operator = ((ComboBox)_ctrl).SelectedItem.ToString();
                                            switch (((ComboBox)_ctrl).SelectedItem.ToString())
                                            {
                                                case "Equals":
                                                    _filterStart += " -eq ";
                                                    break;
                                                case "Not Equal":
                                                    _filterStart += " -ne ";
                                                    break;
                                                case "Contains":
                                                    _filterStart += " -like ";
                                                    break;
                                            }

                                        }
                                        else if (_ctrl.GetType() == typeof(TextBox) && !string.IsNullOrWhiteSpace(_ctrl.Text))
                                        {
                                            _filterStart += _operator == "Contains" ? "'*" + _ctrl.Text + "*' " : "\"" + _ctrl.Text + "\"";
                                        }
                                    }
                                }
                                else if (_pnl.Name == "pnl_FieldMapping")
                                {
                                    int _cntr = 0;
                                    foreach (ComboBox _cmb in _pnl.Controls.OfType<ComboBox>())
                                    {
                                        if(_cmb.Name.Contains("cbx_FieldMapping") && _cmb.SelectedIndex > 0)
                                        {
                                            _fieldMappings.Add(_cmb.SelectedItem.ToString());
                                        }
                                        else if(_cmb.Name.Contains("cbx_PGiMapping") && _cmb.SelectedIndex > 0)
                                        {
                                            _fieldMappings[_cntr] =  _fieldMappings[_cntr] + "," + _cmb.SelectedItem.ToString() + "|";
                                            _cntr += 1;
                                        }
                                    }

                                    if (_fieldMappings.Count() > 0)
                                    {
                                        _fieldMappings[_fieldMappings.Count() - 1] = _fieldMappings[_fieldMappings.Count() - 1].Replace("|", "");
                                    }

                                    foreach (var _s in _fieldMappings)
                                    {
                                        _fieldMappingsFinal += _s;
                                    }
                                    //MessageBox.Show(_fieldMappingsFinal);
                                    //Console.ReadLine();
                                }
                            }
                        }
                    }
                }                

                CreateRegistryKey();

                using (StreamWriter _sw = new StreamWriter(lbl_LogFilePathResult.Text + "\\config\\config.txt"))
                {
                    _sw.WriteLine(txt_CompanyId.Text);
                    _sw.WriteLine(txt_O365Username.Text);
                    _sw.WriteLine(_pass);
                    _sw.WriteLine(txt_APIId.Text);
                    _sw.WriteLine(txt_APIPassword.Text);
                    _sw.WriteLine(lbl_LogFilePathResult.Text);
                    _sw.WriteLine(cbx_DefaultLanguage.SelectedIndex.ToString());
                    _sw.WriteLine(cbx_Env.SelectedIndex.ToString());
                    _sw.WriteLine(_filterStart + _filterEnd);
                    _sw.WriteLine(ckbx_Create.Checked.ToString());
                    _sw.WriteLine(ckbx_Delete.Checked.ToString());
                    _sw.WriteLine(ckbx_DepartmentlBilling.Checked.ToString());
                    _sw.WriteLine(_fieldMappingsFinal);
                    _sw.WriteLine(_deleteFilter + _filterEnd);
                }

                string _cultureName = "";
                ResourceManager _rm = GlobalMeetAutoProvisionerConfig.Resources.ResourceManager;

                switch (cbx_DefaultLanguage.SelectedIndex)
                {
                    case 0:
                        _cultureName = CultureValues.US;
                        break;
                    case 1:
                        _cultureName = CultureValues.US;
                        break;
                    case 2:
                        _cultureName = CultureValues.French;
                        break;
                    case 3:
                        _cultureName = CultureValues.German;
                        break;
                    case 4:
                        _cultureName = CultureValues.Japanese;
                        break;
                }

                MessageBox.Show(_rm.GetString("Saved", CultureInfo.GetCultureInfo(_cultureName)));
                //this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btn_ViewLogs_Click(object sender, EventArgs e)
        {
            if(lbl_LogFilePathResult.Text != "")
            {
                Process.Start("explorer.exe", lbl_LogFilePathResult.Text);
            }
        }

        private string CheckForSpecialChars(string Text)
        {
            string _returnString = "";
            try
            {
                char[] _specialArray = new char[12] { '$', '@', '_', '^', '?', '%', '!', '#', '&', '*', '*', ')' };

                foreach (char _c in Text.ToCharArray())
                {
                    if (_specialArray.Contains(_c))
                    {
                        _returnString += "`" + _c.ToString();
                    }
                    else
                    {
                        _returnString += _c.ToString();
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return _returnString;
        }

        private PowerShell CreatePowershell()
        {
            var sessionState = InitialSessionState.CreateDefault();
            sessionState.ImportPSModule(new[] { "AutoProvisioner" });
            var powerShell = PowerShell.Create();
            powerShell.Runspace = RunspaceFactory.CreateRunspace(sessionState);
            powerShell.Runspace.Open();
            return powerShell;
        }

        private void CreateRegistryKey()
        {
            Microsoft.Win32.RegistryKey key;

            try
            {
                key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\PGI\\AutoProvision\\", Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree);
                if (key == null)
                {
                    key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey("Software\\PGI\\AutoProvision\\");
                }

                if (key.GetValue("Config") != null)
                {
                    key.DeleteValue("Config");
                }

                if (key.GetValue("Config2") != null)
                {
                    key.DeleteValue("Config2");
                }

                key.SetValue("Config", lbl_LogFilePathResult.Text);
                key.SetValue("Config2", txt_O365Password.Text);
                key.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //MessageBox.Show("Closing");
            if(_pid > 0)
            {
                Process.GetProcessById(_pid).Kill();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Microsoft.Win32.RegistryKey key;
            try
            {
                key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\PGI\\AutoProvision");

                if (key != null)
                {
                    Load_Form(key.GetValue("Config").ToString(), key.GetValue("Config2").ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != "Object reference not set to an instance of an object.")
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void Load_Form(string LogFilePath, string Config2)
        {
            try
            {                
                _bgw.WorkerReportsProgress = true;
                _bgw.DoWork += new DoWorkEventHandler(BGW_DoWork);
                //_bgw.ProgressChanged += new ProgressChangedEventHandler(BGW_ProgressChanged);

                if (_env == "qab")
                {
                    _webId = "6993327";
                    _webPw = "v74g08";
                }
                else if (_env == "prod")
                {
                    _webId = "76443560";
                    _webPw = "WrzUtZ0C";
                }

                ResourceManager _rm = GlobalMeetAutoProvisionerConfig.Resources.ResourceManager;

                string[] _config = File.ReadAllLines(LogFilePath + "\\config\\config.txt");
                string _cultureName = "";

                txt_CompanyId.Text = _config[0];
                txt_O365Username.Text = _config[1];
                txt_O365Password.Text = Config2;
                txt_APIId.Text = _config[3];
                txt_APIPassword.Text = _config[4];
                lbl_LogFilePathResult.Text = _config[5];
                cbx_DefaultLanguage.SelectedIndex = _config.Length >= 7 ? Int32.Parse(_config[6]) : 0;
                cbx_Env.SelectedIndex = _config.Length >= 8 ? Int32.Parse(_config[7]) : 0;
                ckbx_Create.Checked = _config.Length >= 10 ? Boolean.Parse(_config[9]) : false;
                ckbx_Delete.Checked = _config.Length >= 11 ? Boolean.Parse(_config[10]) : false;
                ckbx_DepartmentlBilling.Checked = _config.Length >= 12 ? Boolean.Parse(_config[11]) : false;

                switch (cbx_DefaultLanguage.SelectedIndex)
                {
                    case 0:
                        _cultureName = CultureValues.US;
                        break;
                    case 1:
                        _cultureName = CultureValues.US;
                        break;
                    case 2:
                        _cultureName = CultureValues.French;
                        break;
                    case 3:
                        _cultureName = CultureValues.German;
                        break;
                    case 4:
                        _cultureName = CultureValues.Japanese;
                        break;
                }

                ckbx_Create.Text = _rm.GetString("ckbx_Create", CultureInfo.GetCultureInfo(_cultureName));
                ckbx_Delete.Text = _rm.GetString("ckbx_Delete", CultureInfo.GetCultureInfo(_cultureName));
                //grpbx_Filters.Text = _rm.GetString("grpbx_Filter", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_DeptFilter.Text = _rm.GetString("lbl_DeptFilter", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_EmailFilter.Text = _rm.GetString("lbl_EmailFilter", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_RegionFilter.Text = _rm.GetString("lbl_RegionFilter", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_FirstNameFilter.Text = _rm.GetString("lbl_FirstNameFilter", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_LastNameFilter.Text = _rm.GetString("lbl_LastNameFilter", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_TitleFilter.Text = _rm.GetString("lbl_TitleFilter", CultureInfo.GetCultureInfo(_cultureName));
                lbl_CompanyId.Text = _rm.GetString("lbl_CompanyId", CultureInfo.GetCultureInfo(_cultureName));
                lbl_O365Username.Text = _rm.GetString("lbl_O365Username", CultureInfo.GetCultureInfo(_cultureName));
                lbl_O365Password.Text = _rm.GetString("lbl_O365Password", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_WebId.Text = _rm.GetString("lbl_WebId", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_WebPassword.Text = _rm.GetString("lbl_WebPassword", CultureInfo.GetCultureInfo(_cultureName));
                lbl_APIId.Text = _rm.GetString("lbl_APIId", CultureInfo.GetCultureInfo(_cultureName));
                lbl_APIPassword.Text = _rm.GetString("lbl_APIPassword", CultureInfo.GetCultureInfo(_cultureName));
                lbl_DefaultLanguage.Text = _rm.GetString("lbl_DefaultLanguage", CultureInfo.GetCultureInfo(_cultureName));
                lbl_Env.Text = _rm.GetString("lbl_Env", CultureInfo.GetCultureInfo(_cultureName));
                btn_BrowseFolder.Text = _rm.GetString("btn_BrowseFolder", CultureInfo.GetCultureInfo(_cultureName));
                btn_Run.Text = _rm.GetString("btn_Run", CultureInfo.GetCultureInfo(_cultureName));
                btn_ViewLogs.Text = _rm.GetString("btn_ViewLogs", CultureInfo.GetCultureInfo(_cultureName));
                btn_SaveForm.Text = _rm.GetString("btn_SaveForm", CultureInfo.GetCultureInfo(_cultureName));
                btn_Exit.Text = _rm.GetString("btn_Exit", CultureInfo.GetCultureInfo(_cultureName));

                object[] _items = new object[cbx_DefaultLanguage.Items.Count];
                _items[0] = _rm.GetString("SelectOne", CultureInfo.GetCultureInfo(_cultureName));
                _items[1] = _rm.GetString("English", CultureInfo.GetCultureInfo(_cultureName));
                _items[2] = _rm.GetString("French", CultureInfo.GetCultureInfo(_cultureName));
                _items[3] = _rm.GetString("German", CultureInfo.GetCultureInfo(_cultureName));
                _items[4] = _rm.GetString("Japanese", CultureInfo.GetCultureInfo(_cultureName));

                cbx_DefaultLanguage.Items.Clear();
                cbx_DefaultLanguage.Items.AddRange(_items);
                cbx_DefaultLanguage.SelectedIndex = _config.Length >= 7 ? Int32.Parse(_config[6]) : 0;
            }
            catch(Exception ex)
            {

            }
        }

        private void Process_Exited(object sender, EventArgs e)
        {
            foreach (Control ctrl in this.Controls)
            {
                if (ctrl.Name == "pBar_RunProgress")
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        pBar_RunProgress.MarqueeAnimationSpeed = 0;
                        pBar_RunProgress.Style = ProgressBarStyle.Blocks;
                        pBar_RunProgress.Visible = false;
                    }));
                }
            }
            _pid = 0;
        }

        private void cbx_DefaultLanguage_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                ComboBox _cbx = (ComboBox)sender;
                int _selectedIndex = _cbx.SelectedIndex;

                string _cultureName = "";
                switch (cbx_DefaultLanguage.SelectedIndex)
                {
                    case 0:
                        _cultureName = CultureValues.US;
                        break;
                    case 1:
                        _cultureName = CultureValues.US;
                        break;
                    case 2:
                        _cultureName = CultureValues.French;
                        break;
                    case 3:
                        _cultureName = CultureValues.German;
                        break;
                    case 4:
                        _cultureName = CultureValues.Japanese;
                        break;
                }

                ResourceManager _rm = GlobalMeetAutoProvisionerConfig.Resources.ResourceManager;

                ckbx_Create.Text = _rm.GetString("ckbx_Create", CultureInfo.GetCultureInfo(_cultureName));
                ckbx_Delete.Text = _rm.GetString("ckbx_Delete", CultureInfo.GetCultureInfo(_cultureName));
                //grpbx_Filters.Text = _rm.GetString("grpbx_Filter", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_DeptFilter.Text = _rm.GetString("lbl_DeptFilter", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_EmailFilter.Text = _rm.GetString("lbl_EmailFilter", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_RegionFilter.Text = _rm.GetString("lbl_RegionFilter", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_FirstNameFilter.Text = _rm.GetString("lbl_FirstNameFilter", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_LastNameFilter.Text = _rm.GetString("lbl_LastNameFilter", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_TitleFilter.Text = _rm.GetString("lbl_TitleFilter", CultureInfo.GetCultureInfo(_cultureName));
                lbl_CompanyId.Text = _rm.GetString("lbl_CompanyId", CultureInfo.GetCultureInfo(_cultureName));
                lbl_O365Username.Text = _rm.GetString("lbl_O365Username", CultureInfo.GetCultureInfo(_cultureName));
                lbl_O365Password.Text = _rm.GetString("lbl_O365Password", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_WebId.Text = _rm.GetString("lbl_WebId", CultureInfo.GetCultureInfo(_cultureName));
                //lbl_WebPassword.Text = _rm.GetString("lbl_WebPassword", CultureInfo.GetCultureInfo(_cultureName));
                lbl_APIId.Text = _rm.GetString("lbl_APIId", CultureInfo.GetCultureInfo(_cultureName));
                lbl_APIPassword.Text = _rm.GetString("lbl_APIPassword", CultureInfo.GetCultureInfo(_cultureName));
                lbl_DefaultLanguage.Text = _rm.GetString("lbl_DefaultLanguage", CultureInfo.GetCultureInfo(_cultureName));
                lbl_Env.Text = _rm.GetString("lbl_Env", CultureInfo.GetCultureInfo(_cultureName));
                btn_BrowseFolder.Text = _rm.GetString("btn_BrowseFolder", CultureInfo.GetCultureInfo(_cultureName));
                btn_Run.Text = _rm.GetString("btn_Run", CultureInfo.GetCultureInfo(_cultureName));
                btn_ViewLogs.Text = _rm.GetString("btn_ViewLogs", CultureInfo.GetCultureInfo(_cultureName));
                btn_SaveForm.Text = _rm.GetString("btn_SaveForm", CultureInfo.GetCultureInfo(_cultureName));
                btn_Exit.Text = _rm.GetString("btn_Exit", CultureInfo.GetCultureInfo(_cultureName));

                object[] _items = new object[cbx_DefaultLanguage.Items.Count];
                _items[0] = _rm.GetString("SelectOne", CultureInfo.GetCultureInfo(_cultureName));
                _items[1] = _rm.GetString("English", CultureInfo.GetCultureInfo(_cultureName));
                _items[2] = _rm.GetString("French", CultureInfo.GetCultureInfo(_cultureName));
                _items[3] = _rm.GetString("German", CultureInfo.GetCultureInfo(_cultureName));
                _items[4] = _rm.GetString("Japanese", CultureInfo.GetCultureInfo(_cultureName));

                cbx_DefaultLanguage.Items.Clear();
                cbx_DefaultLanguage.Items.AddRange(_items);
                cbx_DefaultLanguage.SelectedIndex = _selectedIndex;
            }
            catch(Exception ex)
            {

            }
        }

        private void btn_Filters_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(lbl_LogFilePathResult.Text + "\\config\\config.txt"))
            {
                if (Application.OpenForms.OfType<Form2>().Count() > 0)
                {
                    Form2 _frm2 = Application.OpenForms.OfType<Form2>().First();
                    _frm2.Show();
                    _frm2.Focus();
                }
                else
                {
                    Form2 _frm2 = new Form2();
                    _frm2.Show();
                    this.Show();
                    _frm2.Focus();
                }
            }
            else
            {
                MessageBox.Show("You must first click the Save button before setting any filters.");
            }
        }
    }
}
