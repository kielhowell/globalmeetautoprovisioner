﻿using System;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Resources;
using System.Globalization;
using System.Diagnostics;
using System.Threading;

namespace GlobalMeetAutoProvisionerConfig
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Label lbl_SaveFilters = null;
                Label lbl_SaveFieldMappings = null;

                DialogResult _result = MessageBox.Show("Would you like to save these filters and field mappings?", "Confirm", MessageBoxButtons.YesNo);

                if (_result == DialogResult.Yes)
                {
                    foreach (Form _frm in Application.OpenForms)
                    {
                        if (_frm.GetType() == typeof(Form1))
                        {
                            lbl_SaveFilters = (Label)_frm.Controls.Find("lbl_SaveFilters", true)[0];
                            lbl_SaveFieldMappings = (Label)_frm.Controls.Find("lbl_SaveFieldMappings", true)[0];
                        }

                    }

                    if (lbl_SaveFilters != null && lbl_SaveFieldMappings != null)
                    {
                        Control _pnlFiltersLastControl = pnl_Filters.Controls.OfType<TextBox>().Where(w => !String.IsNullOrWhiteSpace(w.Text)).LastOrDefault();
                        Control _pnlFieldMappingLastControl = pnl_FieldMapping.Controls.OfType<ComboBox>().Where(w => w.SelectedIndex > 0).LastOrDefault();

                        lbl_SaveFilters.Text = "";
                        lbl_SaveFieldMappings.Text = "";

                        foreach (Control _ctrl in pnl_Filters.Controls)
                        {
                            if (_ctrl.GetType() == typeof(ComboBox) && ((ComboBox)_ctrl).SelectedIndex > 0)
                            {
                                lbl_SaveFilters.Text += _ctrl.Name + ":" + ((ComboBox)_ctrl).SelectedIndex.ToString() + "|";
                            }
                            if (_ctrl.GetType() == typeof(TextBox) && !String.IsNullOrWhiteSpace(_ctrl.Text))
                            {
                                lbl_SaveFilters.Text += _ctrl.Name + ":" + _ctrl.Text + (_ctrl == _pnlFiltersLastControl ? "" : "|");
                            }
                        }
                        foreach (Control _ctrl in pnl_FieldMapping.Controls)
                        {
                            if (_ctrl.GetType() == typeof(ComboBox) && ((ComboBox)_ctrl).SelectedIndex > 0)
                            {
                                lbl_SaveFieldMappings.Text += _ctrl.Name + ":" + ((ComboBox)_ctrl).SelectedIndex.ToString() + (_ctrl == _pnlFieldMappingLastControl ? "" : "|");
                            }
                        }
                    }
                }
                e.Cancel = true;
                this.Hide();
                   
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                btn_RemoveFilter.Enabled = false;
                cbx_AndOr.Enabled = false;
                cbx_AndOr.SelectedIndex = 0;
                cbx_Filters.SelectedIndex = 0;
                cbx_FiltersOperators.SelectedIndex = 0;
                cbx_FieldMapping.SelectedIndex = 0;
                cbx_PGiMapping.SelectedIndex = 0;
                btn_RemoveFieldMapping.Enabled = false;

                CheckExistingFiltersAndFieldMappings();

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btn_AddFieldMapping_Click(object sender, EventArgs e)
        {
            int _cntrlNumber = ((pnl_FieldMapping.Controls.Count) / 3);
            int _cntrlPrevNumber = _cntrlNumber - 1;
            string _cntrlPrevNumberString = _cntrlPrevNumber > 0 ? _cntrlPrevNumber.ToString() : "";

            gpbx_FieldMaps.Size = new Size(gpbx_FieldMaps.Size.Width, gpbx_FieldMaps.Height + 29);
            //gpbx_FieldMaps.Location = new Point(gpbx_FieldMaps.Location.X, gpbx_FieldMaps.Location.Y + 29);
            CreateControl(new ComboBox(), "pnl_FieldMapping", "cbx_FieldMapping" + _cntrlNumber.ToString(), "cbx_FieldMapping" + _cntrlPrevNumberString, cbx_FieldMapping.Items.Cast<string>().ToList(), false);
            CreateControl(new ComboBox(), "pnl_FieldMapping", "cbx_PGiMapping" + _cntrlNumber.ToString(), "cbx_PGiMapping" + _cntrlPrevNumberString, cbx_PGiMapping.Items.Cast<string>().ToList(), false);
            CreateControl(new Label(), "pnl_FieldMapping", "lbl_MapsTo" + _cntrlNumber.ToString(), "lbl_MapsTo" + _cntrlPrevNumberString, null, true);
        }

        private void btn_AddFilter_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(pnl_Filters.Controls.Count.ToString());
            int _cntrlNumber = ((pnl_Filters.Controls.Count) / 4);
            int _cntrlPrevNumber = _cntrlNumber - 1;
            string _cntrlPrevNumberString = _cntrlPrevNumber > 0 ? _cntrlPrevNumber.ToString() : "";

            gpbx_Filters.Size = new Size(gpbx_Filters.Size.Width, gpbx_Filters.Height + 29);
            gpbx_FieldMaps.Location = new Point(gpbx_FieldMaps.Location.X, gpbx_FieldMaps.Location.Y + 29);

            List<string> _cbxAndOrItems = new List<string>(new string[] { "And", "Or" });

            CreateControl(new ComboBox(), "pnl_Filters", "cbx_AndOr" + _cntrlNumber.ToString(), "cbx_AndOr" + _cntrlPrevNumberString, _cbxAndOrItems, false);
            CreateControl(new ComboBox(), "pnl_Filters", "cbx_Filters" + _cntrlNumber.ToString(), "cbx_Filters" + _cntrlPrevNumberString, cbx_Filters.Items.Cast<string>().ToList(), false);
            CreateControl(new ComboBox(), "pnl_Filters", "cbx_FiltersOperators" + _cntrlNumber.ToString(), "cbx_FiltersOperators" + _cntrlPrevNumberString, cbx_FiltersOperators.Items.Cast<string>().ToList(), false);
            CreateControl(new TextBox(), "pnl_Filters", "txt_FiltersValue" + _cntrlNumber.ToString(), "txt_FiltersValue" + _cntrlPrevNumberString, null, true);
        }

        private void btn_RemoveFieldMapping_Click(object sender, EventArgs e)
        {
            List<Control> _ctrls = new List<Control>();
            foreach (Control c in pnl_FieldMapping.Controls)
            {
                _ctrls.Add(c);
            }

            int _ctrlNum = (((pnl_FieldMapping.Controls.Count) / 3) - 1);

            _ctrls.RemoveAll(r => r.Name == "cbx_FieldMapping" + _ctrlNum.ToString());
            _ctrls.RemoveAll(r => r.Name == "cbx_PGiMapping" + _ctrlNum.ToString());
            _ctrls.RemoveAll(r => r.Name == "lbl_MapsTo" + _ctrlNum.ToString());

            pnl_FieldMapping.Controls.Clear();

            List<Control> _ctrlArray = new List<Control>();
            foreach (Control _ctrl in _ctrls)
            {
                _ctrlArray.Add(_ctrl);
            }

            pnl_FieldMapping.Controls.AddRange(_ctrlArray.ToArray());

            if (_ctrlNum == 1)
            {
                btn_RemoveFieldMapping.Enabled = false;
            }

            btn_AddFieldMapping.Location = new Point(btn_AddFieldMapping.Left, btn_AddFieldMapping.Top - 29);
            btn_RemoveFieldMapping.Location = new Point(btn_RemoveFieldMapping.Left, btn_RemoveFieldMapping.Top - 29);
            //gpbx_FieldMaps.Size = new Size(gpbx_FieldMaps.Size.Width, gpbx_FieldMaps.Size.Height - 29);
            //pnl_FieldMapping.Size = new Size(pnl_FieldMapping.Size.Width, pnl_FieldMapping.Size.Height - 29);
            gpbx_FieldMaps.Location = new Point(gpbx_FieldMaps.Location.X, gpbx_Filters.Bottom + 5);
            //pnl_FieldMapping.Location = new Point(pnl_FieldMapping.Location.X, pnl_FieldMapping.Location.Y - 29);
            
            Form2 _frm = Application.OpenForms.OfType<Form2>().FirstOrDefault();

            if ((gpbx_Filters.Height + gpbx_FieldMaps.Height) >= (_frm.MinimumSize.Height - 71))
            {                
                _frm.Size = new Size(_frm.Size.Width, _frm.Height - 29);
                //lbl_Copyright.Location = new Point(lbl_Copyright.Location.X, _frm.Height - 71);
            }
            else
            {
                _frm.Size = _frm.MinimumSize;
            }

            _frm.Update();
            //gpbx_FieldMaps.Update();
            //gpbx_FieldMaps.Refresh();
            //pnl_FieldMapping.Refresh();
            _frm.Refresh();
        }

        private void btn_RemoveFilter_Click(object sender, EventArgs e)
        {
            List<Control> _ctrls = new List<Control>();
            foreach(Control c in pnl_Filters.Controls)
            {
                _ctrls.Add(c);
            }
            
            int _ctrlNum = (((pnl_Filters.Controls.Count) / 4) - 1);

            _ctrls.RemoveAll(r => r.Name == "cbx_AndOr" + _ctrlNum.ToString());
            _ctrls.RemoveAll(r => r.Name == "cbx_Filters" + _ctrlNum.ToString());
            _ctrls.RemoveAll(r => r.Name == "cbx_FiltersOperators" + _ctrlNum.ToString());
            _ctrls.RemoveAll(r => r.Name == "txt_FiltersValue" + _ctrlNum.ToString());

            pnl_Filters.Controls.Clear();

            List<Control> _ctrlArray = new List<Control>();
            foreach(Control _ctrl in _ctrls)
            {
                _ctrlArray.Add(_ctrl);
            }

            pnl_Filters.Controls.AddRange(_ctrlArray.ToArray());

            if (_ctrlNum == 1)
            {
                btn_RemoveFilter.Enabled = false;
            }

            btn_AddFilter.Location = new Point(btn_AddFilter.Left, btn_AddFilter.Top - 29);
            btn_RemoveFilter.Location = new Point(btn_RemoveFilter.Left, btn_RemoveFilter.Top - 29);
            //gpbx_Filters.Height = gpbx_Filters.Size.Height - 29;
            //pnl_Filters.Size = new Size(pnl_Filters.Size.Width, pnl_Filters.Size.Height - 29);
            gpbx_FieldMaps.Location = new Point(gpbx_FieldMaps.Location.X, gpbx_Filters.Bottom + 5);
            //pnl_FieldMapping.Location = new Point(pnl_FieldMapping.Location.X, pnl_FieldMapping.Location.Y - 29);

            Form2 _frm = Application.OpenForms.OfType<Form2>().FirstOrDefault();

            if ((gpbx_Filters.Height + gpbx_FieldMaps.Height) >= (_frm.MinimumSize.Height - 71))
            {   
                //lbl_Copyright.Location = new Point(lbl_Copyright.Location.X, _frm.Height - 100);
                _frm.Size = new Size(_frm.Width, (_frm.Height - 29));
                _frm.Update();
                _frm.Refresh();
            }
            else
            {
                _frm.Size = _frm.MinimumSize;
            }
            //gpbx_Filters.Update();
            //gpbx_Filters.Refresh();
            //pnl_Filters.Refresh();
        }

        private void CheckExistingFiltersAndFieldMappings()
        {
            Label lbl_SaveFilters = null;
            Label lbl_SaveFieldMappings = null;

            foreach (Form _frm in Application.OpenForms)
            {
                if (_frm.GetType() == typeof(Form1))
                {
                    lbl_SaveFilters = (Label)_frm.Controls.Find("lbl_SaveFilters", true)[0];
                    lbl_SaveFieldMappings = (Label)_frm.Controls.Find("lbl_SaveFieldMappings", true)[0];
                }

            }

            if (lbl_SaveFilters != null && !string.IsNullOrWhiteSpace(lbl_SaveFilters.Text))
            {
                Dictionary<string, string> _filters = new Dictionary<string, string>();
                _filters = lbl_SaveFilters.Text.Split('|').ToDictionary(d => d.Split(':')[0], d => d.Split(':')[1]);

                foreach (var _kvp in _filters)
                {
                    if (pnl_Filters.Controls.IndexOfKey(_kvp.Key) >= 0)
                    {
                        Control _ctrl = pnl_Filters.Controls.Find(_kvp.Key, true)[0];

                        if (_ctrl != null)
                        {
                            if (_ctrl.GetType() == typeof(ComboBox))
                            {
                                ((ComboBox)_ctrl).SelectedIndex = Int32.Parse(_kvp.Value);
                            }
                            else if (_ctrl.GetType() == typeof(TextBox))
                            {
                                ((TextBox)_ctrl).Text = _kvp.Value;
                            }
                        }
                    }
                    else
                    {
                        btn_AddFilter.PerformClick();
                        Control _ctrl = pnl_Filters.Controls.Find(_kvp.Key, true)[0];
                        if (_ctrl.GetType() == typeof(ComboBox))
                        {
                            ((ComboBox)_ctrl).SelectedIndex = Int32.Parse(_kvp.Value);
                        }
                        else if (_ctrl.GetType() == typeof(TextBox))
                        {
                            ((TextBox)_ctrl).Text = _kvp.Value;
                        }
                    }
                }
            }
            if (lbl_SaveFieldMappings != null && !string.IsNullOrWhiteSpace(lbl_SaveFieldMappings.Text))
            {
                Dictionary<string, string> _fieldMappings = new Dictionary<string, string>();
                _fieldMappings = lbl_SaveFieldMappings.Text.Split('|').ToDictionary(d => d.Split(':')[0], d => d.Split(':')[1]);

                foreach (var _kvp in _fieldMappings)
                {
                    if (pnl_FieldMapping.Controls.IndexOfKey(_kvp.Key) >= 0)
                    {
                        Control _ctrl = pnl_FieldMapping.Controls.Find(_kvp.Key, true)[0];

                        if (_ctrl != null)
                        {
                            if (_ctrl.GetType() == typeof(ComboBox))
                            {
                                ((ComboBox)_ctrl).SelectedIndex = Int32.Parse(_kvp.Value);
                            }
                            else if (_ctrl.GetType() == typeof(TextBox))
                            {
                                ((TextBox)_ctrl).Text = _kvp.Value;
                            }
                        }
                    }
                    else
                    {
                        btn_AddFieldMapping.PerformClick();
                        Control _ctrl = pnl_FieldMapping.Controls.Find(_kvp.Key, true)[0];
                        if (_ctrl.GetType() == typeof(ComboBox))
                        {
                            ((ComboBox)_ctrl).SelectedIndex = Int32.Parse(_kvp.Value);
                        }
                        else if (_ctrl.GetType() == typeof(TextBox))
                        {
                            ((TextBox)_ctrl).Text = _kvp.Value;
                        }
                    }
                }
            }
        }

        private void CreateControl(Control _control, string panelName, string controlName, string controlPrevName, List<string> comboBoxValues = null, bool _redraw = false)
        {
            Panel _pnl = (Panel)this.Controls.Find(panelName, true)[0];            
            Control _prevCntrl = _pnl.Controls.Find(controlPrevName, false)[0];

            if (_control.GetType() == typeof(TextBox))
            {
                ((TextBox)_control).Multiline = true;
            }

            _control.Name = controlName;
            _control.Height = _prevCntrl.Height;
            _control.Width = _prevCntrl.Width;
            _control.Size = _prevCntrl.Size;

            if(_control.GetType() == typeof(Label))
            {
                _control.Text = "Maps To";
            }

            if (comboBoxValues != null)
            {
                foreach (string _value in comboBoxValues)
                {
                    ((ComboBox)_control).Items.Add(_value);
                }
                ((ComboBox)_control).SelectedIndex = 0;
            }
                
            _control.Location = new Point(_prevCntrl.Left, _prevCntrl.Bottom + 5);
           
            _pnl.Controls.Add(_control);

            if (_redraw)
            {
                foreach (Button _btn in _pnl.Parent.Controls.OfType<Button>())
                {
                    _btn.Location = new Point(_btn.Left, _pnl.Bottom + 5);
                    _btn.Enabled = true;
                }
                
                _pnl.Height = _pnl.Height + _control.Height;
                _pnl.Refresh();
                _pnl.Parent.Height = _pnl.Parent.Height + _control.Height;
                _pnl.Parent.Refresh();
                _pnl.Parent.Update();

                Form2 _frm = Application.OpenForms.OfType<Form2>().FirstOrDefault();
                if ((gpbx_Filters.Size.Height + gpbx_FieldMaps.Size.Height) >= (_frm.MinimumSize.Height - 71))
                {
                    _frm.Size = new Size(_frm.Size.Width, (gpbx_Filters.Size.Height + gpbx_FieldMaps.Size.Height)+71);
                    //lbl_Copyright.Location = new Point(lbl_Copyright.Location.X, _frm.Height - 71);
                    _frm.Refresh();
                    _frm.Update();
                }
                else
                {
                    _frm.Size = _frm.MinimumSize;
                }
            }
        }

        //private void GroupBox_Resize(object sender, EventArgs e)
        //{
            //if(gpbx_Filters.Height + gpbx_FieldMaps.Height >= this.MinimumSize.Height)
            //{
            //    this.Height = gpbx_Filters.Height + gpbx_FieldMaps.Height;
            //    lbl_Copyright.Location = new Point(lbl_Copyright.Location.X, gpbx_FieldMaps.Bottom + 5);
            //}
        //}
    }
}
