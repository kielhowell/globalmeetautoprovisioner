﻿namespace GlobalMeetAutoProvisionerConfig
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbx_Filters = new System.Windows.Forms.GroupBox();
            this.btn_RemoveFilter = new System.Windows.Forms.Button();
            this.pnl_Filters = new System.Windows.Forms.Panel();
            this.cbx_Filters = new System.Windows.Forms.ComboBox();
            this.cbx_FiltersOperators = new System.Windows.Forms.ComboBox();
            this.txt_FiltersValue = new System.Windows.Forms.TextBox();
            this.btn_AddFilter = new System.Windows.Forms.Button();
            this.gpbx_FieldMaps = new System.Windows.Forms.GroupBox();
            this.pnl_FieldMapping = new System.Windows.Forms.Panel();
            this.lbl_MapsTo = new System.Windows.Forms.Label();
            this.cbx_FieldMapping = new System.Windows.Forms.ComboBox();
            this.cbx_PGiMapping = new System.Windows.Forms.ComboBox();
            this.btn_RemoveFieldMapping = new System.Windows.Forms.Button();
            this.btn_AddFieldMapping = new System.Windows.Forms.Button();
            this.cbx_AndOr = new System.Windows.Forms.ComboBox();
            this.gpbx_Filters.SuspendLayout();
            this.pnl_Filters.SuspendLayout();
            this.gpbx_FieldMaps.SuspendLayout();
            this.pnl_FieldMapping.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpbx_Filters
            // 
            this.gpbx_Filters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpbx_Filters.AutoSize = true;
            this.gpbx_Filters.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gpbx_Filters.Controls.Add(this.btn_RemoveFilter);
            this.gpbx_Filters.Controls.Add(this.pnl_Filters);
            this.gpbx_Filters.Controls.Add(this.btn_AddFilter);
            this.gpbx_Filters.Location = new System.Drawing.Point(12, 9);
            this.gpbx_Filters.Margin = new System.Windows.Forms.Padding(0);
            this.gpbx_Filters.MinimumSize = new System.Drawing.Size(600, 100);
            this.gpbx_Filters.Name = "gpbx_Filters";
            this.gpbx_Filters.Padding = new System.Windows.Forms.Padding(0);
            this.gpbx_Filters.Size = new System.Drawing.Size(600, 110);
            this.gpbx_Filters.TabIndex = 22;
            this.gpbx_Filters.TabStop = false;
            this.gpbx_Filters.Text = "Filters";
            // 
            // btn_RemoveFilter
            // 
            this.btn_RemoveFilter.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RemoveFilter.Location = new System.Drawing.Point(52, 62);
            this.btn_RemoveFilter.Name = "btn_RemoveFilter";
            this.btn_RemoveFilter.Size = new System.Drawing.Size(30, 30);
            this.btn_RemoveFilter.TabIndex = 7;
            this.btn_RemoveFilter.Text = "-";
            this.btn_RemoveFilter.UseVisualStyleBackColor = true;
            this.btn_RemoveFilter.Click += new System.EventHandler(this.btn_RemoveFilter_Click);
            // 
            // pnl_Filters
            // 
            this.pnl_Filters.AutoSize = true;
            this.pnl_Filters.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnl_Filters.Controls.Add(this.cbx_AndOr);
            this.pnl_Filters.Controls.Add(this.cbx_Filters);
            this.pnl_Filters.Controls.Add(this.cbx_FiltersOperators);
            this.pnl_Filters.Controls.Add(this.txt_FiltersValue);
            this.pnl_Filters.Location = new System.Drawing.Point(8, 17);
            this.pnl_Filters.Name = "pnl_Filters";
            this.pnl_Filters.Size = new System.Drawing.Size(581, 28);
            this.pnl_Filters.TabIndex = 6;
            // 
            // cbx_Filters
            // 
            this.cbx_Filters.FormattingEnabled = true;
            this.cbx_Filters.Items.AddRange(new object[] {
            "- Select One -",
            "Department",
            "Email",
            "First Name",
            "Last Name",
            "Region",
            "Title",
            "UsageLocation"});
            this.cbx_Filters.Location = new System.Drawing.Point(83, 0);
            this.cbx_Filters.Name = "cbx_Filters";
            this.cbx_Filters.Size = new System.Drawing.Size(162, 24);
            this.cbx_Filters.TabIndex = 4;
            // 
            // cbx_FiltersOperators
            // 
            this.cbx_FiltersOperators.FormattingEnabled = true;
            this.cbx_FiltersOperators.Items.AddRange(new object[] {
            "- Select One -",
            "Equals",
            "Contains",
            "Not Equal"});
            this.cbx_FiltersOperators.Location = new System.Drawing.Point(258, 0);
            this.cbx_FiltersOperators.Name = "cbx_FiltersOperators";
            this.cbx_FiltersOperators.Size = new System.Drawing.Size(113, 24);
            this.cbx_FiltersOperators.TabIndex = 3;
            // 
            // txt_FiltersValue
            // 
            this.txt_FiltersValue.Location = new System.Drawing.Point(386, 1);
            this.txt_FiltersValue.MaximumSize = new System.Drawing.Size(192, 24);
            this.txt_FiltersValue.MinimumSize = new System.Drawing.Size(192, 24);
            this.txt_FiltersValue.Multiline = true;
            this.txt_FiltersValue.Name = "txt_FiltersValue";
            this.txt_FiltersValue.Size = new System.Drawing.Size(192, 24);
            this.txt_FiltersValue.TabIndex = 2;
            // 
            // btn_AddFilter
            // 
            this.btn_AddFilter.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddFilter.Location = new System.Drawing.Point(16, 62);
            this.btn_AddFilter.Name = "btn_AddFilter";
            this.btn_AddFilter.Size = new System.Drawing.Size(30, 30);
            this.btn_AddFilter.TabIndex = 5;
            this.btn_AddFilter.Text = "+";
            this.btn_AddFilter.UseVisualStyleBackColor = true;
            this.btn_AddFilter.Click += new System.EventHandler(this.btn_AddFilter_Click);
            // 
            // gpbx_FieldMaps
            // 
            this.gpbx_FieldMaps.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpbx_FieldMaps.AutoSize = true;
            this.gpbx_FieldMaps.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gpbx_FieldMaps.Controls.Add(this.pnl_FieldMapping);
            this.gpbx_FieldMaps.Controls.Add(this.btn_RemoveFieldMapping);
            this.gpbx_FieldMaps.Controls.Add(this.btn_AddFieldMapping);
            this.gpbx_FieldMaps.Location = new System.Drawing.Point(12, 119);
            this.gpbx_FieldMaps.Margin = new System.Windows.Forms.Padding(0);
            this.gpbx_FieldMaps.MinimumSize = new System.Drawing.Size(600, 100);
            this.gpbx_FieldMaps.Name = "gpbx_FieldMaps";
            this.gpbx_FieldMaps.Padding = new System.Windows.Forms.Padding(0);
            this.gpbx_FieldMaps.Size = new System.Drawing.Size(600, 110);
            this.gpbx_FieldMaps.TabIndex = 23;
            this.gpbx_FieldMaps.TabStop = false;
            this.gpbx_FieldMaps.Text = "Field Mapping";
            // 
            // pnl_FieldMapping
            // 
            this.pnl_FieldMapping.AutoSize = true;
            this.pnl_FieldMapping.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnl_FieldMapping.Controls.Add(this.lbl_MapsTo);
            this.pnl_FieldMapping.Controls.Add(this.cbx_FieldMapping);
            this.pnl_FieldMapping.Controls.Add(this.cbx_PGiMapping);
            this.pnl_FieldMapping.Location = new System.Drawing.Point(14, 20);
            this.pnl_FieldMapping.Name = "pnl_FieldMapping";
            this.pnl_FieldMapping.Size = new System.Drawing.Size(575, 28);
            this.pnl_FieldMapping.TabIndex = 9;
            // 
            // lbl_MapsTo
            // 
            this.lbl_MapsTo.Location = new System.Drawing.Point(249, 1);
            this.lbl_MapsTo.Name = "lbl_MapsTo";
            this.lbl_MapsTo.Size = new System.Drawing.Size(74, 24);
            this.lbl_MapsTo.TabIndex = 10;
            this.lbl_MapsTo.Text = "Maps To";
            // 
            // cbx_FieldMapping
            // 
            this.cbx_FieldMapping.FormattingEnabled = true;
            this.cbx_FieldMapping.Items.AddRange(new object[] {
            "- Select One -",
            "AccountEnabled",
            "AssignedLicenses",
            "AssignedPlans",
            "City",
            "CompanyName",
            "Country",
            "Department",
            "DisplayName",
            "ExtensionProperty",
            "FacsimileTelephoneNumber",
            "GivenName",
            "JobTitle",
            "Mail",
            "MailNickName",
            "Mobile",
            "OnPremisesSecurityIdentifier",
            "OtherMails",
            "PasswordPolicies",
            "PasswordProfile",
            "PhysicalDeliveryOfficeName",
            "PostalCode",
            "PreferredLanguage",
            "ProvisionedPlans",
            "ProvisioningErrors",
            "ProxyAddresses",
            "SignInNames",
            "SipProxyAddress",
            "State",
            "StreetAddress",
            "Surname",
            "TelephoneNumber",
            "UsageLocation",
            "UserPrincipalName",
            "UserType"});
            this.cbx_FieldMapping.Location = new System.Drawing.Point(7, 1);
            this.cbx_FieldMapping.Name = "cbx_FieldMapping";
            this.cbx_FieldMapping.Size = new System.Drawing.Size(176, 24);
            this.cbx_FieldMapping.TabIndex = 4;
            // 
            // cbx_PGiMapping
            // 
            this.cbx_PGiMapping.FormattingEnabled = true;
            this.cbx_PGiMapping.Items.AddRange(new object[] {
            "- Select One -",
            "City",
            "Company",
            "Country",
            "Department",
            "DisplayName",
            "Email",
            "Extension",
            "Fax",
            "Mobile",
            "PostalCode",
            "State",
            "StreetAddress",
            "Title"});
            this.cbx_PGiMapping.Location = new System.Drawing.Point(396, 1);
            this.cbx_PGiMapping.Name = "cbx_PGiMapping";
            this.cbx_PGiMapping.Size = new System.Drawing.Size(176, 24);
            this.cbx_PGiMapping.TabIndex = 3;
            // 
            // btn_RemoveFieldMapping
            // 
            this.btn_RemoveFieldMapping.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RemoveFieldMapping.Location = new System.Drawing.Point(52, 62);
            this.btn_RemoveFieldMapping.Name = "btn_RemoveFieldMapping";
            this.btn_RemoveFieldMapping.Size = new System.Drawing.Size(30, 30);
            this.btn_RemoveFieldMapping.TabIndex = 7;
            this.btn_RemoveFieldMapping.Text = "-";
            this.btn_RemoveFieldMapping.UseVisualStyleBackColor = true;
            this.btn_RemoveFieldMapping.Click += new System.EventHandler(this.btn_RemoveFieldMapping_Click);
            // 
            // btn_AddFieldMapping
            // 
            this.btn_AddFieldMapping.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddFieldMapping.Location = new System.Drawing.Point(16, 62);
            this.btn_AddFieldMapping.Name = "btn_AddFieldMapping";
            this.btn_AddFieldMapping.Size = new System.Drawing.Size(30, 30);
            this.btn_AddFieldMapping.TabIndex = 5;
            this.btn_AddFieldMapping.Text = "+";
            this.btn_AddFieldMapping.UseVisualStyleBackColor = true;
            this.btn_AddFieldMapping.Click += new System.EventHandler(this.btn_AddFieldMapping_Click);
            // 
            // cbx_AndOr
            // 
            this.cbx_AndOr.FormattingEnabled = true;
            this.cbx_AndOr.Items.AddRange(new object[] {
            "",
            "And",
            "Or"});
            this.cbx_AndOr.Location = new System.Drawing.Point(8, 0);
            this.cbx_AndOr.Name = "cbx_AndOr";
            this.cbx_AndOr.Size = new System.Drawing.Size(65, 24);
            this.cbx_AndOr.TabIndex = 5;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(626, 555);
            this.Controls.Add(this.gpbx_FieldMaps);
            this.Controls.Add(this.gpbx_Filters);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(644, 600);
            this.Name = "Form2";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "GlobalMeet Auto Provisioner Config";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.Load += new System.EventHandler(this.Form2_Load);
            this.gpbx_Filters.ResumeLayout(false);
            this.gpbx_Filters.PerformLayout();
            this.pnl_Filters.ResumeLayout(false);
            this.pnl_Filters.PerformLayout();
            this.gpbx_FieldMaps.ResumeLayout(false);
            this.gpbx_FieldMaps.PerformLayout();
            this.pnl_FieldMapping.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox gpbx_Filters;
        private System.Windows.Forms.Button btn_AddFilter;
        private System.Windows.Forms.ComboBox cbx_Filters;
        private System.Windows.Forms.ComboBox cbx_FiltersOperators;
        private System.Windows.Forms.Panel pnl_Filters;
        private System.Windows.Forms.Button btn_RemoveFilter;
        private System.Windows.Forms.GroupBox gpbx_FieldMaps;
        private System.Windows.Forms.Button btn_RemoveFieldMapping;
        private System.Windows.Forms.ComboBox cbx_FieldMapping;
        private System.Windows.Forms.ComboBox cbx_PGiMapping;
        private System.Windows.Forms.Button btn_AddFieldMapping;
        private System.Windows.Forms.Panel pnl_FieldMapping;
        private System.Windows.Forms.Label lbl_MapsTo;
        private System.Windows.Forms.TextBox txt_FiltersValue;
        private System.Windows.Forms.ComboBox cbx_AndOr;
    }
}

