﻿using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace GlobalMeetAutoProvisioner
{
    public class HmacTokenizer
    {
        public string ApiId { get; set; }
        public string ApiPassword { get; set; }
        public string LogOnId { get; set; }
        public string LogOnPassword { get; set; }
        public int TokenDuration { get; set; }

        public HmacTokenizer(string api, string apiPassword, string logOnId, string logOnPassword)
        {
            ApiId = api;
            ApiPassword = apiPassword;
            LogOnId = logOnId;
            LogOnPassword = logOnPassword;
            TokenDuration = (10 * 60 * 60);
        }

        public string GetTokenClient()
        {
            var expireDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, DateTime.UtcNow.Hour,
                 DateTime.UtcNow.Minute, 0).AddSeconds(TokenDuration);

            var hmacMessage = GetHash(LogOnId, ApiId, GetAuthKey(LogOnPassword, ApiPassword), expireDate);
            return String.Format(CultureInfo.InvariantCulture, "w{0},c{1},{2},{3}", ApiId, LogOnId,
                              expireDate.ToString("yyyyMMddHHmm", CultureInfo.InvariantCulture), hmacMessage);
        }

        private static string GetHash(string logOnId, string apiId, byte[] authKey, DateTime expireDate)
        {
            var encoding = new UTF8Encoding();
            var authText = "\r\nWebId\": \"" + apiId + "\"," +
                           "\r\nLogOnId\": \"" + logOnId + "\"," +
                           "\r\nTokenDate\": \"" + expireDate.ToString("yyyyMMdd", CultureInfo.InvariantCulture) +
                           "\"," +
                           "\r\nTokenTime\": \"" + expireDate.ToString("HHmm", CultureInfo.InvariantCulture) +
                           "\",\r\n";
            var hmac = new HMACSHA1(authKey);
            var messageBytes = encoding.GetBytes(authText);
            return ByteToString(hmac.ComputeHash(messageBytes));
        }


        private static byte[] GetAuthKey(string logonPassword, string apiPassword)
        {
            var encoding = new UTF8Encoding();
            var authKey = String.Format(CultureInfo.InvariantCulture, "{0},{1}", apiPassword, logonPassword);
            return encoding.GetBytes(authKey);
        }

        private static string ByteToString(byte[] buff)
        {
            var sbinary = "";
            for (var i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2", CultureInfo.InvariantCulture);
            }
            return (sbinary);
        }

    }

}
