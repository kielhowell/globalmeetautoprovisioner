﻿using GlobalMeetAutoProvisioner;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Net.Http;
using System.Resources;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlobalMeetAutoProvisioner
{
    class Program
    {
        #region global
        private string _apiId = "";
        private string _apiPw = "";
        private int _companyId = 0;
        private bool _create = false;
        private string _cultureName = "en-US";
        private string _defaultLanguage = "";
        private bool _delete = false;
        private List<string> _disabledUsers = new List<string>();
        private List<string> _enabledUsers = new List<string>();
        private string _env = "QAB";
        private Dictionary<string,string> _fieldMapping = new Dictionary<string,string>();
        private string _filter = " -filter {AcpInfo -eq $null} ";
        private string _deleteFilter = "";
        private string _logfilePath = "";
        private string _hmacToken = "";
        private string _options = "";
        private string _passcodeReqs = "EnhancedAuthenticationGlobalMeetDisabled";
        private string _passwordFileName = "";
        private string _passwordFilePath = "";
        private string _passwordFilePathName = "";
        private string _provisionResponse = "";
        private string _username = "";
        private bool _splitBilling = false;
        private string _pass = "";

        private const string _webIdQAB = "6993327";
        private const string _webPwQAB = "v74g08";
        private const string _webIdPROD = "76443560";
        private const string _webPwPROD = "WrzUtZ0C";

        private Dictionary<string, string> _clients = new Dictionary<string, string>();
        private bool interactive = true;
        private ResourceManager _rm = GlobalMeetAutoProvisioner.Resources.ResourceManager;
        #endregion

        static void Main(string[] args)
        {
            try
            {
                var _nonACP = new Program();

                _nonACP.CheckRegistryConfig();

                //Detecting if command line parameters were passed in and parsing, either a credentials file (from a manual run) or flags for other things.
                #region non-interactive
                if (args.Length > 0)
                {
                    List<string> _args = args.ToList();

                    _nonACP.interactive = false;

                    int _testNum = 0;
                    //if (_args.IndexOf("-ApiId") > -1 && Int32.TryParse(_args[_args.IndexOf("-ApiId") + 1], out _testNum))
                    //{
                    _nonACP._apiId = _args[_args.IndexOf("-ApiId") + 1];
                    //}
                    if (_args.IndexOf("-ApiPw") > -1)
                    {
                        //_nonACP._webPw = _args[_args.IndexOf("-ApiPw") + 1];
                    }
                    //if (_args.IndexOf("-WebId") > -1 && Int32.TryParse(_args[_args.IndexOf("-WebId")+1], out _testNum))
                    //{
                    //_nonACP._webId = _args[_args.IndexOf("-WebId") + 1];
                    //
                    //if (_args.IndexOf("-WebPw") > -1)
                    //{
                    //    _nonACP._webPw = _args[_args.IndexOf("-WebPw") + 1];
                    //}
                    if (_args.IndexOf("-AppUsername") > -1)
                    {
                        _nonACP._username = _args[_args.IndexOf("-AppUsername") + 1];
                    }
                    if (_args.IndexOf("-AppPw") > -1)
                    {
                        _nonACP._pass = _nonACP.CheckForSpecialChars(_args[_args.IndexOf("-AppPw") + 1]);
                    }
                    if (_args.IndexOf("-CompanyId") > -1 && Int32.TryParse(_args[_args.IndexOf("-CompanyId") + 1], out _testNum))
                    {
                        _nonACP._companyId = Int32.Parse(_args[_args.IndexOf("-CompanyId") + 1]);
                    }

                    if (_args.IndexOf("-Language") > -1)
                    {
                        switch (args[_args.IndexOf("-LogFilePath") + 1])
                        {
                            case "English":
                                _nonACP._cultureName = "en-US";
                                break;
                            case "French":
                                _nonACP._cultureName = "fr-FR";
                                break;
                            case "German":
                                _nonACP._cultureName = "de-DE";
                                break;
                            case "Japanese":
                                _nonACP._cultureName = "ja-JA";
                                break;
                        }
                    }

                    if (_args.IndexOf("-Environ") > -1)
                    {
                        _nonACP._env = args[_args.IndexOf("-Environ") + 1];
                    }

                    if (_args.IndexOf("-ConfigFile") > -1)
                    {
                        _nonACP._passwordFilePathName = args[_args.IndexOf("-ConfigFile") + 1];
                        string[] _configFile = File.ReadAllLines(_nonACP._passwordFilePathName); //Credentials file is built automatically through interactive mode.
                        _nonACP._companyId = Int32.Parse(_configFile[0].Replace(System.Environment.NewLine, "")); //doing Parse because to get into the file it had to pass the TryParse
                        _nonACP._username = _configFile[1].Replace(System.Environment.NewLine, "");
                        _nonACP._pass = _configFile[2].Replace(System.Environment.NewLine, "");
                        _nonACP._apiId = _configFile[3].Replace(System.Environment.NewLine, "");
                        _nonACP._apiPw = _configFile[4].Replace(System.Environment.NewLine, "");

                        switch (_configFile[7])
                        {
                            case "1":
                                _nonACP._env = "QAB";
                                break;
                            case "2":
                                _nonACP._env = "PROD";
                                break;
                        }

                        if (_configFile.Length >= 9)
                        {
                            if (_configFile[8].Replace(System.Environment.NewLine, "") != "")
                            {
                                _nonACP._filter = _configFile[8].Replace(System.Environment.NewLine, "");
                            }
                        }

                        _nonACP._create = true;
                        _nonACP._delete = true;

                        if (_configFile.Length >= 14)
                        {
                            if (_configFile[13].Replace(System.Environment.NewLine, "") != "")
                            {
                                _nonACP._deleteFilter = _configFile[13].Replace(System.Environment.NewLine, "");
                            }
                        }
                        //_nonACP._webId = _configFile[3].Replace(System.Environment.NewLine, ""); 
                        //_nonACP._webPw = _configFile[4].Replace(System.Environment.NewLine, "");
                    }
                    //Default to C:\temp for logging.
                    if (_args.IndexOf("-LogFilePath") > -1)
                    {
                        _nonACP._logfilePath = args[_args.IndexOf("-LogFilePath") + 1];
                        if (_nonACP._logfilePath.Substring(_nonACP._logfilePath.Length - 1) != "\\")
                        {
                            _nonACP._logfilePath += "\\";
                        }
                    }
                    else
                    {
                        _nonACP._logfilePath = "C:\\temp\\";
                    }

                    if (_args.IndexOf("-Interactive") > -1)
                    {
                        bool _testInteractive = false;
                        if (Boolean.TryParse(args[_args.IndexOf("-Interactive") + 1], out _testInteractive))
                        {
                            _nonACP.interactive = Convert.ToBoolean(args[_args.IndexOf("-Interactive") + 1]);
                        }
                    }
                }
                #endregion

                //Set up config values in interactive mode and generating credentials file.
                _nonACP.GetConfigValues();
                _nonACP.GetEnabledUsers();

                if (_nonACP._create)
                {
                    _nonACP._options = _nonACP.GetCompanyPGiOptions();
                    #region New Users
                    var users = _nonACP.GetNonACP();
                    List<ACPUser> _acp = new List<ACPUser>();

                    foreach (string user in users)
                    {
                        if (user.Split(',')[0].Replace("sip:", "").Length <= 75) //REST api does not allow emails > 75 chars
                        {
                            ACPUser _acpUser = new ACPUser();
                            
                            _acpUser = _nonACP.ProvisionPGi(user);
                            if (_acpUser == null)
                            {
                                return;
                            }

                            _acp.Add(_acpUser);
                        }
                    }

                    using (StreamWriter _sw = new StreamWriter(_nonACP._logfilePath + "ProvisionNonACP_Log_" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                    {
                        _sw.WriteLine(_nonACP._provisionResponse);
                    }
                    _nonACP._provisionResponse = "";

                    if (_nonACP.interactive)
                    {
                        Program _pr = new Program();
                        if (_acp.Count() > 0)
                        {
                            _nonACP.UpdateACP(_acp);
                            Console.WriteLine(_pr._rm.GetString("Finished", CultureInfo.GetCultureInfo(_pr._cultureName)));
                            Console.ReadLine();
                        }
                        else
                        {
                            Console.WriteLine(_pr._rm.GetString("NoUsers", CultureInfo.GetCultureInfo(_pr._cultureName)));
                            Console.ReadLine();
                        }
                    }
                    else
                    {
                        if (_acp.Count() > 0)
                        {
                            _nonACP.UpdateACP(_acp);
                        }
                    }
                    #endregion
                }

                //Get all O365 users and PGi, check to see if any PGi provisioned users exist that are not in the O365 set, deactivate the delta
                if (_nonACP._delete)
                {
                    #region Deactivate Users           
                    List<string> _o365 = _nonACP.GetO365();

                    _nonACP._clients = _nonACP.GetPGi();
                    //Dictionary<string, string> _inactiveClients = new Dictionary<string, string>();
                    List<string> _clientIds = new List<string>();
                    string _confList = "";
                    if (_o365 != null && _o365.Count() > 0)
                    {
                        foreach (string email in _nonACP._clients.Values)
                        {
                            if (_o365.Contains(email))
                            {
                                _clientIds.Add(_nonACP._clients.FirstOrDefault(f => f.Value == email && f.Key != null).Key);
                                _confList += email + System.Environment.NewLine;
                            }
                        }
                        
                        DialogResult _mboxVal = AutoCloseMessageBox.Show("Press OK for the following emails to be deactivated: " + System.Environment.NewLine + _confList, "Confirmation", 10000, MessageBoxButtons.OKCancel);
                        MessageBox.Show("MessageBox after AutoCloseMessageBox");
                        if (_mboxVal == DialogResult.OK)
                        {
                            if (_clientIds != null && _clientIds.Count() > 0)
                            {
                                _nonACP.DeactivatePGi(_clientIds.ToArray());
                            }
                        }
                    }

                    if (_nonACP._disabledUsers.Count > 0)
                    {
                        foreach(string email in _nonACP._disabledUsers)
                        {
                            if (_nonACP._clients.Values.Contains(email))
                            {
                                _clientIds.Add(_nonACP._clients.FirstOrDefault(f => f.Value == email && f.Key != null).Key);
                                _confList += email + System.Environment.NewLine;
                            }
                        }

                        DialogResult _mboxVal = AutoCloseMessageBox.Show("Press OK for the following emails to be deactivated: " + System.Environment.NewLine + _confList, "Confirmation", 10000, MessageBoxButtons.OKCancel);

                        if (_mboxVal == DialogResult.OK)
                        {
                            _nonACP.RemoveACP(_nonACP._disabledUsers);
                        }

                        if (_clientIds != null && _clientIds.Count() > 0)
                        {
                            _nonACP.DeactivatePGi(_clientIds.ToArray());
                        }
                    }

                    #endregion
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void CheckRegistryConfig()
        {
            Microsoft.Win32.RegistryKey key;
            try
            {
                key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\PGI\\AutoProvision");

                if (key != null)
                {
                    _passwordFilePathName = key.GetValue("Config").ToString() + "\\config\\config.txt";
                    string[] _config = File.ReadAllLines(_passwordFilePathName);
                    //MessageBox.Show("Company Id: " + _config[0].ToString());
                    _companyId = Int32.Parse(_config[0].Replace(System.Environment.NewLine, "")); //always an int from the config program
                    _username = _config[1].Replace(System.Environment.NewLine, "");
                    _pass = CheckForSpecialChars(key.GetValue("Config2").ToString());                    
                    _apiId = _config[3].Replace(System.Environment.NewLine, "");
                    _apiPw = _config[4].Replace(System.Environment.NewLine, "");
                    _logfilePath = _config[5].Replace(System.Environment.NewLine, "") + "\\";

                    switch (_config[6])
                    {
                        case "1":
                            _cultureName = "en-US";
                            break;
                        case "2":
                            _cultureName = "fr-FR";
                            break;
                        case "3":
                            _cultureName = "de-DE";
                            break;
                        case "4":
                            _cultureName = "ja-JA";
                            break;
                    }

                    switch(_config[7])
                    {
                        case "1":
                            _env = "QAB";
                            break;
                        case "2":
                            _env = "PROD";
                            break;
                    }

                    if(_config[8].Replace(System.Environment.NewLine,"") != "")
                    {
                        _filter = _config[8].Replace(System.Environment.NewLine, "");
                    }

                    _create = Boolean.Parse(_config[9]);
                    _delete = Boolean.Parse(_config[10]);
                    _splitBilling = Boolean.Parse(_config[11]);

                    if (_config.Length >= 13)
                    {
                        if (_config[12].Replace(System.Environment.NewLine, "") != "")
                        {
                            foreach (var kvp in _config[12].Split('|'))
                            {
                                string[] _tempKvp = kvp.Split(',');
                                _fieldMapping.Add(_tempKvp[0], _tempKvp[1]);
                            }
                        }
                    }

                    if (_config.Length >= 14)
                    {
                        if (_config[13].Replace(System.Environment.NewLine, "") != "")
                        {
                            _deleteFilter = _config[13].Replace(System.Environment.NewLine, "");
                        }
                    }

                    interactive = false;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != "Object reference not set to an instance of an object.")
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void GetConfigValues()
        {
            #region interactive
            if (interactive)
            {
                Program _pr = new Program();
                Console.WriteLine("© 2017 Premiere Global Services, Inc. and/or its affiliates");

                int _langNum = 0;
                Console.WriteLine("Please select a number for default language: ");
                Console.WriteLine("1) English ");
                Console.WriteLine("2) French ");
                Console.WriteLine("3) German ");
                Console.WriteLine("4) Japanese ");

                if (Int32.TryParse(Console.ReadLine(), out _langNum))
                {
                    switch (_langNum)
                    {
                        case 1:
                            _pr._cultureName = CultureValues.US;
                            break;
                        case 2:
                            _pr._cultureName = CultureValues.French;
                            break;
                        case 3:
                            _pr._cultureName = CultureValues.German;
                            break;
                        case 4:
                            _pr._cultureName = CultureValues.Japanese;
                            Console.OutputEncoding = System.Text.Encoding.UTF8;
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Language selection was not a valid integer. Press any key to exit.");
                    Console.ReadLine();
                    Environment.Exit(0);
                }

                int _envNum = 0;
                Console.WriteLine(_pr._rm.GetString("Environment", CultureInfo.GetCultureInfo(_pr._cultureName)));
                Console.WriteLine("1) QAB");
                Console.WriteLine("2) PROD");
                _env = Console.ReadLine();

                if(!Int32.TryParse(_env, out _envNum))
                {
                    Environment.Exit(0);
                }
                else
                {
                    switch(_envNum)
                    {
                        case 1:
                            _env = "QAB";
                            break;
                        case 2:
                            _env = "PROD";
                            break;
                    }
                }

                Console.WriteLine(_pr._rm.GetString("LoggingPath", CultureInfo.GetCultureInfo(_pr._cultureName)));
                _logfilePath = Console.ReadLine().Replace("\\\\", "\\");

                if (_logfilePath.Replace(System.Environment.NewLine,"").Substring(_logfilePath.Length - 1) != "\\")
                {
                    _logfilePath += "\\";
                }

                Console.WriteLine(_pr._rm.GetString("CredentialsPath", CultureInfo.GetCultureInfo(_pr._cultureName)));
                _passwordFilePath = Console.ReadLine().Replace("\\\\", "\\");
                Console.WriteLine(_pr._rm.GetString("CredentialsFile", CultureInfo.GetCultureInfo(_pr._cultureName)));
                _passwordFileName = Console.ReadLine();                

                Console.WriteLine(_pr._rm.GetString("CheckingCredentials", CultureInfo.GetCultureInfo(_pr._cultureName)));

                if (_passwordFilePath.Replace(System.Environment.NewLine, "").Substring(_passwordFilePath.Length - 1) != "\\")
                {
                    _passwordFilePath += "\\";
                }
                _passwordFilePathName = _passwordFilePath + _passwordFileName;

                string _tempPasswordFilePath = _passwordFilePath;
                if (!System.IO.File.Exists(_passwordFilePathName.Replace("\\\\", "\\")))
                {
                    Console.WriteLine(_pr._rm.GetString("CredentialsDoesNotExist", CultureInfo.GetCultureInfo(_pr._cultureName)));
                    if (!System.IO.Directory.Exists(_passwordFilePath))
                    {
                        System.IO.DirectoryInfo _di = new System.IO.DirectoryInfo(_passwordFilePath);
                        _di.Create();
                    }

                    int _parseNum = 0;                    
                    int _parseWebId = 0;                    

                    Console.WriteLine(_pr._rm.GetString("CompanyId", CultureInfo.GetCultureInfo(_pr._cultureName)));

                    if (Int32.TryParse(Console.ReadLine(), out _parseNum))
                    {
                        _companyId = _parseNum;
                    }
                    else
                    {
                        Console.WriteLine(_pr._rm.GetString("CompanyIdNotAnInteger", CultureInfo.GetCultureInfo(_pr._cultureName)));
                        Console.ReadLine();
                        //If CompanyId is not 32 bit integer then we want to exit.
                        Environment.Exit(0);
                    }

                    Console.WriteLine(_pr._rm.GetString("APIId", CultureInfo.GetCultureInfo(_pr._cultureName)));

                    //if (Int32.TryParse(Console.ReadLine(), out _parseNum))
                    //{
                    _apiId = Console.ReadLine();
                    //}
                    //else
                    //{
                    //    Console.WriteLine(_pr._rm.GetString("APIIdNotInteger", CultureInfo.GetCultureInfo(_pr._cultureName)));
                    //    Console.ReadLine();
                    //    //If CompanyId is not 32 bit integer then we want to exit.
                    //    Environment.Exit(0);
                    //}

                    Console.WriteLine(_pr._rm.GetString("APIPassword", CultureInfo.GetCultureInfo(_pr._cultureName)));

                    _apiPw = Console.ReadLine();

                    //Console.WriteLine(_pr._rm.GetString("WebId", CultureInfo.GetCultureInfo(_pr._cultureName)));

                    //if (Int32.TryParse(Console.ReadLine(), out _parseWebId))
                    //{
                    //_webId = Console.ReadLine();
                    //}
                    //else
                    //{
                    //    Console.WriteLine(_pr._rm.GetString("WebIdNotAnInteger", CultureInfo.GetCultureInfo(_pr._cultureName)));
                    //    Console.ReadLine();
                    //    //If WebId is not 32 bit integer then we want to exit.
                    //    Environment.Exit(0);
                    //}

                    //Console.WriteLine(_pr._rm.GetString("WebPassword", CultureInfo.GetCultureInfo(_pr._cultureName)));

                    //_webPw = Console.ReadLine();

                    Console.WriteLine(_pr._rm.GetString("O365Username", CultureInfo.GetCultureInfo(_pr._cultureName)));
                    _username = Console.ReadLine();

                    Console.WriteLine(_pr._rm.GetString("O365Password", CultureInfo.GetCultureInfo(_pr._cultureName)));
                    _pass = CheckForSpecialChars(Console.ReadLine());

                    using (PowerShell ps = CreatePowershell())
                    {
                        //Getting password as secure string in PS to store in credentials file.
                        ps.AddScript(string.Format("$pw=\"{0}\" | ConvertTo-SecureString -AsPlainText -Force| convertfrom-securestring;echo $pw", _pass)); //| out-file \"{1}\" -Force   , _passwordFilePathName 
                        ps.Invoke();

                        foreach (var _psItem in ps.Invoke())
                        {
                            if (_psItem != null)
                            {
                                _pass = _psItem.ToString();

                                //Generate credentials file.
                                using (StreamWriter _sw = new StreamWriter(_passwordFilePathName))
                                {
                                    _sw.WriteLine(_companyId);
                                    _sw.WriteLine(_username);
                                    _sw.WriteLine(_psItem.ToString());
                                    //_sw.WriteLine(_webId);
                                    //_sw.WriteLine(_webPw);
                                    _sw.WriteLine(_apiId);
                                    _sw.WriteLine(_apiPw);
                                    _sw.WriteLine(_logfilePath);
                                    _sw.WriteLine(_langNum.ToString());
                                    _sw.WriteLine((_envNum - 1).ToString());
                                }
                            }
                        }
                        ps.Runspace.Close();
                        ps.Runspace.Dispose();
                    }

                }
                else
                {
                    Console.WriteLine(_pr._rm.GetString("CredentialsFilepathFound", CultureInfo.GetCultureInfo(_pr._cultureName)));
                    Console.ReadLine();

                    string[] _configFile = File.ReadAllLines(_passwordFilePathName); //Credentials file is generated automatically from interactive mode.
                    _companyId = Int32.Parse(_configFile[0].Replace(System.Environment.NewLine, "")); //doing Parse because to get into the file it had to pass the TryParse
                    _username = _configFile[1].Replace(System.Environment.NewLine, "");
                    _pass = _configFile[2].Replace(System.Environment.NewLine, "");
                    //_webId = _configFile[3].Replace(System.Environment.NewLine, "");
                    //_webPw = _configFile[4].Replace(System.Environment.NewLine, "");
                    _apiId = _configFile[3].Replace(System.Environment.NewLine, "");
                    _apiPw = _configFile[4].Replace(System.Environment.NewLine, "");
                    _logfilePath = _configFile[5].Replace(System.Environment.NewLine, "");
                    
                    switch (_configFile[6])
                    {
                        case "1":
                            _cultureName = "en-US";
                            break;
                        case "2":
                            _cultureName = "fr-FR";
                            break;
                        case "3":
                            _cultureName = "de-DE";
                            break;
                        case "4":
                            _cultureName = "ja-JA";
                            break;
                    }
                    
                    switch (_configFile[7])
                    {
                        case "1":
                            _env = "QAB";
                            break;
                        case "2":
                            _env = "PROD";
                            break;
                    }

                    if (_configFile.Length >= 9)
                    {
                        if (_configFile[8].Replace(System.Environment.NewLine, "") != "")
                        {
                            _filter = _configFile[8].Replace(System.Environment.NewLine, "");
                        }
                    }
                    _create = true;
                    _delete = true;
                }
            }
            #endregion
            #region non-interactive
            else
            {
                using (PowerShell ps = CreatePowershell())
                {
                    //Convert plain text password to secure string in PS to use in O365 Powershell modules
                    ps.AddScript(string.Format("$pw=\"{0}\" | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString;echo $pw", _pass)); //| out-file \"{1}\" -Force   , _passwordFilePathName 
                    ps.Invoke();

                    foreach (var _psItem in ps.Invoke())
                    {
                        if (_psItem != null)
                        {
                            _pass = _psItem.ToString();
                        }
                    }
                    ps.Runspace.Close();
                    ps.Runspace.Dispose();
                }
            }
            #endregion
            //TODO: Change API Id and API Pw for production.
            //Hmac token generation.
            HmacTokenizer _hmac = null;
            if (_env == "QAB")
            {
                _hmac = new HmacTokenizer(_apiId.ToString(), _apiPw, _webIdQAB.ToString(), _webPwQAB.ToString());
            }
            else if(_env == "PROD")
            {
                _hmac = new HmacTokenizer(_webIdPROD.ToString(), _webPwPROD.ToString(), _apiId.ToString(), _apiPw);
            }
            _hmacToken = _hmac.GetTokenClient();            
        }

        //Check for special reserved characters in PS and put a tick mark for PS escape char.
        private string CheckForSpecialChars(string Text)
        {
            string _returnString = "";
            char[] _specialArray = new char[12] {'$','@', '_', '^', '?', '%', '!', '#', '&', '*', '*', ')' };

            foreach(char _c in Text.ToCharArray())
            {
                if(_specialArray.Contains(_c))
                {
                    _returnString += "`" + _c.ToString();
                }
                else
                {
                    _returnString += _c.ToString();
                }
            }

            return _returnString;
        }

        //Create PS
        private PowerShell CreatePowershell()
        {
            var sessionState = InitialSessionState.CreateDefault();
            sessionState.ImportPSModule(new[] { "ACP" });
            var powerShell = PowerShell.Create();
            powerShell.Runspace = RunspaceFactory.CreateRunspace(sessionState);
            powerShell.Runspace.Open();
            return powerShell;
        }

        //Get O365 enabled users
        private void GetEnabledUsers()
        {
            try
            {
                using (PowerShell ps = CreatePowershell())
                {
                    ps.Runspace.SessionStateProxy.SetVariable("myUsername", _username);
                    ps.Runspace.SessionStateProxy.SetVariable("myPassword", _pass);
                    ps.Commands.AddScript("Set-ExecutionPolicy Unrestricted -force");
                    ps.Commands.AddScript("enable-psremoting -force");
                    ps.Commands.AddScript("set-winrmnetworkdelayms 30000");
                    ps.Commands.AddScript("if(get-packageprovider|where-object{$_.Name -eq \"NuGet\"}){}else{Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force}");
                    ps.Commands.AddScript("if(get-command|where-object{$_.Name -eq \"Connect-AzureAD\"}){}else{Install-Module AzureAD -Force}");
                    ps.Commands.AddScript("$myPassword = $myPassword | convertto-securestring");
                    ps.Commands.AddScript("$credentials = new-object -typename System.Management.Automation.PSCredential -argumentlist $myUsername,$myPassword");
                    ps.Commands.AddScript("Import-Module AzureAD");
                    ps.Commands.AddScript("Connect-AzureAD -credential $credentials");
                    ps.Commands.AddScript("$global:enabledUsers = Get-AzureADUser -filter 'accountEnabled eq true'|select UserPrincipalName");
                    ps.Commands.AddScript("ForEach ($enabled in $enabledUsers) {$total=$enabled.UserPrincipalName;echo $total}");

                    foreach (var _psItem in ps.Invoke())
                    {
                        if (_psItem != null && !_psItem.ToString().ToLower().Contains("pgibot"))
                        {
                            _enabledUsers.Add(_psItem.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                using (StreamWriter _sw = new StreamWriter(_logfilePath + "ProvisionerError_" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                {
                    _sw.WriteLine(ex.ToString());
                }
            }
        }

        //Get O365 disabled users
        private void GetDisabledO365Users()
        {

            try
            {
                using (PowerShell ps = CreatePowershell())
                {
                    string _tempFilter = " -filter \"accountEnabled eq false ";
                    string _userPrincipalName = "";
                    if (!string.IsNullOrWhiteSpace(_filter) && _filter.Contains("UserPrincipalName"))
                    {
                        foreach(string _line in _filter.Split('\"'))
                        {
                            if(_line.Contains('@'))
                            {
                                _userPrincipalName = _line;
                            }
                        }

                        _tempFilter += " and UserPrincipalName eq \'" + _userPrincipalName + "\'\"";
                    }
                    else
                    {
                        _tempFilter += "\"";
                    }

                    ps.Runspace.SessionStateProxy.SetVariable("myUsername", _username);
                    ps.Runspace.SessionStateProxy.SetVariable("myPassword", _pass);
                    ps.Commands.AddScript("Set-ExecutionPolicy Unrestricted -force");
                    ps.Commands.AddScript("enable-psremoting -force");
                    ps.Commands.AddScript("set-winrmnetworkdelayms 30000");
                    ps.Commands.AddScript("if(get-packageprovider|where-object{$_.Name -eq \"NuGet\"}){}else{Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force}");
                    ps.Commands.AddScript("if(get-command|where-object{$_.Name -eq \"Connect-AzureAD\"}){}else{Install-Module AzureAD -Force}");
                    ps.Commands.AddScript("$myPassword = $myPassword | convertto-securestring");
                    ps.Commands.AddScript("$credentials = new-object -typename System.Management.Automation.PSCredential -argumentlist $myUsername,$myPassword");
                    ps.Commands.AddScript("Import-Module AzureAD");
                    ps.Commands.AddScript("Connect-AzureAD -credential $credentials");
                    ps.Commands.AddScript("$global:disabledUsers = Get-AzureADUser " + _tempFilter + "|select UserPrincipalName");

                    //using (StreamWriter _s = new StreamWriter(_logfilePath + "Get-AzureADUserFilter.txt"))
                    //{
                    //    _s.WriteLine("$global:disabledUsers = Get-AzureADUser " + _tempFilter + "|select UserPrincipalName");
                    //}

                    ps.Commands.AddScript("ForEach ($disabled in $disabledUsers) {$total=$disabled.UserPrincipalName;echo $total}");

                    foreach (var _psItem in ps.Invoke())
                    {
                        if (_psItem != null && !_psItem.ToString().Contains("pgibot"))
                        {
                            _disabledUsers.Add(_psItem.ToString().ToLower().Replace("sip:", ""));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                using (StreamWriter _sw = new StreamWriter(_logfilePath + "ProvisionerError_" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                {
                    _sw.WriteLine(ex.ToString());
                }
            }
        }

        //Gets O365 users without ACP information via O365 PS modules
        private List<string> GetNonACP()
        {
            List<string> _nonACPUsers = new List<string>();
            List<string> _usersFinal = new List<string>();
            
            try
            {
                using (PowerShell ps = CreatePowershell())
                {
                    ps.Runspace.SessionStateProxy.SetVariable("myUsername", _username);
                    ps.Runspace.SessionStateProxy.SetVariable("myPassword", _pass);
                    ps.Commands.AddScript("Set-ExecutionPolicy Unrestricted -force");
                    ps.Commands.AddScript("enable-psremoting -force");
                    ps.Commands.AddScript("set-winrmnetworkdelayms 30000");                   
                    ps.Commands.AddScript("$myPassword = $myPassword | convertto-securestring");
                    ps.Commands.AddScript("$credentials = new-object -typename System.Management.Automation.PSCredential -argumentlist $myUsername,$myPassword");
                    ps.Commands.AddScript("Import-Module SkypeOnlineConnector");
                    ps.Commands.AddScript("$session = New-CsOnlineSession -Credential $credentials -Verbose");
                    ps.Commands.AddScript("Import-PSSession -Session $session");
                    ps.Commands.AddScript("Import-Module MsOnline");
                    ps.Commands.AddScript("Connect-MsolService -Credential $credentials");                    ;
                    ps.Commands.AddScript("$global:usersWithoutACP = Get-CsOnlineUser" + _filter);
                    ps.Commands.AddScript("ForEach ($userWithoutACP in $usersWithoutACP) {$total=$userWithoutACP.SipAddress+\",\"+$userWithoutACP.FirstName+\",\"+$userWithoutACP.LastName+\",\"+$userWithoutACP.CountryOrRegionDisplayName+\",\"+$userWithoutACP.Company+\",\"+$userWithoutACP.Department+\",\"+$userWithoutACP.City+\",\"+$userWithoutACP.Office+\",\"+$userWithoutACP.PostalCode+\",\"+$userWithoutACP.StateOrProvince+\",\"+$userWithoutACP.Street+\",\"+$userWithoutACP.StreetAddress+\",\"+$userWithoutACP.Title;echo $total}");

                    foreach (var _psItem in ps.Invoke())
                    {
                        if (_psItem != null && !_psItem.ToString().Contains("pgibot"))
                        {
                            _nonACPUsers.Add(_psItem.ToString());
                        }
                    }                                        

                    foreach (string user in _nonACPUsers)
                    {
                        if (_enabledUsers.Contains(user.Split(',')[0].ToLower().Replace("sip:","")))
                        {
                            _usersFinal.Add(user);
                        }
                    }
                    
                    ps.Commands.AddScript("Remove-PSSession -Session $session");
                    ps.Invoke();
                    ps.Runspace.Close();
                    ps.Runspace.Dispose();
                }
            }
            catch(Exception ex)
            {
                using (StreamWriter _sw = new StreamWriter(_logfilePath + "ProvisionerError_" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                {
                    _sw.WriteLine(ex.ToString());
                }
            }
            return _nonACPUsers;
        }

        private void RemoveACP(List<string> users)
        {
            try
            {
                using (PowerShell ps = CreatePowershell())
                {
                    ps.Runspace.SessionStateProxy.SetVariable("myUsername", _username);
                    ps.Runspace.SessionStateProxy.SetVariable("myPassword", _pass);
                    ps.Commands.AddScript("Set-ExecutionPolicy Unrestricted -force");
                    ps.Commands.AddScript("enable-psremoting -force");
                    ps.Commands.AddScript("set-winrmnetworkdelayms 30000");
                    ps.Commands.AddScript("$myPassword = $myPassword | convertto-securestring");
                    ps.Commands.AddScript("$credentials = new-object -typename System.Management.Automation.PSCredential -argumentlist $myUsername,$myPassword");
                    ps.Commands.AddScript("Import-Module SkypeOnlineConnector");
                    ps.Commands.AddScript("$session = New-CsOnlineSession -Credential $credentials -Verbose");
                    ps.Commands.AddScript("Import-PSSession -Session $session");
                    ps.Commands.AddScript("Import-Module MsOnline");
                    ps.Commands.AddScript("Connect-MsolService -Credential $credentials");

                    foreach (string user in users)
                    {
                        ps.Commands.AddScript("Get-CsOnlineUser -Filter {UserPrincipalName -eq \"" + user + "\"}|Remove-CsUserAcp" );
                    }

                    ps.Commands.AddScript("Remove-PSSession -Session $session");
                    ps.Invoke();
                    ps.Runspace.Close();
                    ps.Runspace.Dispose();
                }
            }
            catch(Exception ex)
            {
                using (StreamWriter _sw = new StreamWriter(_logfilePath + "ProvisionNonACP_Errors_" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                {
                    _sw.WriteLine(ex.ToString());
                }
            }
        }
        //Update O365 ACP information from PGi provisioning via O365 PS Modules
        private void UpdateACP (List<ACPUser> users)
        {
            try
            {
                using (PowerShell ps = CreatePowershell())
                {
                    ps.Runspace.SessionStateProxy.SetVariable("myUsername", _username);
                    ps.Runspace.SessionStateProxy.SetVariable("myPassword", _pass);
                    ps.Commands.AddScript("Set-ExecutionPolicy Unrestricted -force");
                    ps.Commands.AddScript("enable-psremoting -force");
                    ps.Commands.AddScript("set-winrmnetworkdelayms 30000");
                    ps.Commands.AddScript("$myPassword = $myPassword | convertto-securestring");
                    ps.Commands.AddScript("$credentials = new-object -typename System.Management.Automation.PSCredential -argumentlist $myUsername,$myPassword");
                    ps.Commands.AddScript("Import-Module SkypeOnlineConnector");
                    ps.Commands.AddScript("$session = New-CsOnlineSession -Credential $credentials -Verbose");
                    ps.Commands.AddScript("Import-PSSession -Session $session");
                    ps.Commands.AddScript("Import-Module MsOnline");
                    ps.Commands.AddScript("Connect-MsolService -Credential $credentials");

                    foreach (var _user in users)
                    {
                        if (!_user.Email.Contains("pgibot"))
                        {
                            List<string> _tfNums = _user.TollFreeNumbers.Count > 5 ? _user.TollFreeNumbers.Take(5).ToList() : _user.TollFreeNumbers;
                            string _tollFree = _tfNums.Count() > 0 ? "-TollFreeNumbers \"" + String.Join("\",\"", _tfNums) + "\"": "";
                            string _setUserACPCommand = string.Format("Set-CsUserACP -Identity \"{0}\" {1} -TollNumber \"{2}\" -ParticipantPassCode \"{3}\" -Domain \"{4}\" -Name \"{5}\" -Url \"{6}\""
                                , "sip:" + _user.Email.Replace(System.Environment.NewLine, "").Replace(" ", "")
                                , _tollFree
                                , _user.TollNumbers.FirstOrDefault().Replace(System.Environment.NewLine, "").Replace(" ", "")
                                , _user.ParticipantPasscode.Replace(System.Environment.NewLine, "").Replace(" ", "")
                                , "lync.pgi.com"//_user.Domain.Replace(System.Environment.NewLine, "").Replace(" ", "")
                                , "PGi"
                                , "http://www.pgi.com/lynconline/localnums.php");//_user.URL.Replace(System.Environment.NewLine, "").Replace(" ", ""));

                            ps.Commands.AddScript(_setUserACPCommand);
                        }
                    }

                    ps.Commands.AddScript("Remove-PSSession -Session $session");
                    ps.Invoke();
                    ps.Runspace.Close();
                    ps.Runspace.Dispose();
                }
            }
            catch (Exception ex)
            {
                using (StreamWriter _sw = new StreamWriter(_logfilePath + "ProvisionNonACP_Errors_" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                {
                    _sw.WriteLine(ex.ToString());
                } 

            }
        }
        
        //Extract JSON field data
        private static string ExtractJsonField(string result, string element, bool ignoreArray = true)
        {
            // This is done dynamically because PGI is in progress of rebuilding this service to provide consumable schemas
            var jsonObject = Newtonsoft.Json.Linq.JObject.Parse(result);
            var jsonElement = jsonObject.SelectToken(element);
            string _tempJsonElement = "";
            if (jsonElement != null)
            {
                if(!ignoreArray && jsonElement.Type == Newtonsoft.Json.Linq.JTokenType.Array)
                {
                    foreach(var elem in jsonElement)
                    {
                        _tempJsonElement += elem.ToString();
                    }
                }

                if (!ignoreArray && _tempJsonElement != "")
                {
                    return _tempJsonElement;
                }
                else if(ignoreArray && jsonElement.Type == Newtonsoft.Json.Linq.JTokenType.Array)
                {
                    return jsonElement.ToString().Replace(System.Environment.NewLine, "");
                }
                else
                {
                    return (string)jsonElement;
                }
            }
            return null;
        }

        //Extract JSON data as a dictionary
        private static Dictionary<string,string> ExtractJsonFieldStringArray(string result, string arrayToken, string tokenKey, string tokenValue)
        {
            // This is done dynamically because PGI is in progress of rebuilding this service to provide consumable schemas
            Dictionary<string,string> _strings = new Dictionary<string,string>();
            var jsonObject = Newtonsoft.Json.Linq.JObject.Parse(result);
            var jsonElement = jsonObject.SelectToken(arrayToken);
            if (jsonElement != null)
            {

                foreach (var elem in jsonElement)
                {
                    try
                    {
                        _strings.Add((string)elem.SelectToken(tokenValue), (string)elem.SelectToken(tokenKey));
                    }
                    catch(Exception ex)
                    {
                        //continue loop
                    }
                }
            }
            return _strings;
        }

        //Extract JSON error fields
        private static List<string> ExtractJsonFieldErrors(string result)
        {
            // This is done dynamically because PGI is in progress of rebuilding this service to provide consumable schemas
            List<string> _errors = new List<string>();
            var jsonObject = Newtonsoft.Json.Linq.JObject.Parse(result);
            var jsonElement = jsonObject.SelectToken("ClientCreateResult.Errors");
            if (jsonElement != null)
            {

                foreach (var elem in jsonElement)
                {
                    _errors.Add(string.Format("Code: {0}, Message: {1}, Parameter: {2}, ParameterValue: {3}, Severity: {4}, Source: {5}"
                        , (string)elem.SelectToken("Code")
                        , (string)elem.SelectToken("Message")
                        , (string)elem.SelectToken("Parameter")
                        , (string)elem.SelectToken("ParameterValue")
                        , (string)elem.SelectToken("Severity")
                        , (string)elem.SelectToken("Source")
                        ));
                }
            }
            return _errors;
        }

        //Extract JSON Toll Number
        private static List<string> ExtractJsonFieldTollNumber(string result)
        {
            // This is done dynamically because PGI is in progress of rebuilding this service to provide consumable schemas
            List<string> _nums = new List<string>();
            var jsonObject = Newtonsoft.Json.Linq.JObject.Parse(result);
            var jsonElement = jsonObject.SelectToken("ClientCreateResult.Reservation.PhoneNumbers");
            if (jsonElement != null)
            {

                foreach (var elem in jsonElement)
                {
                    if((Int32)elem.SelectToken("PhoneType") == 2 || (Int32)elem.SelectToken("PhoneType") == 4 || (Int32)elem.SelectToken("PhoneType") == 14)
                    {
                        _nums.Add((string)elem.SelectToken("Number"));
                    }
                }                
            }
            return _nums;
        }

        //Extract JSON Toll Free Number
        private static Dictionary<string, string> ExtractJsonFieldTollFreeNumber(string result)
        {
            // This is done dynamically because PGI is in progress of rebuilding this service to provide consumable schemas
            Dictionary<string,string> _nums = new Dictionary<string,string>();
            var jsonObject = Newtonsoft.Json.Linq.JObject.Parse(result);
            var jsonElement = jsonObject.SelectToken("ClientCreateResult.Reservation.PhoneNumbers");
            if (jsonElement != null)
            {

                foreach (var elem in jsonElement)
                {
                    if ((Int32)elem.SelectToken("PhoneType") == 1 || (Int32)elem.SelectToken("PhoneType") == 7)
                    {
                        _nums.Add((string)elem.SelectToken("Number"), (string)elem.SelectToken("Location"));
                    }
                }
            }
            return _nums;
        }

        //Deactivate PGi provisioned users that no longer exist in O365 user set.
        private void DeactivatePGi(string[] clientIds)
        {
            Program _pr = new Program();

            try
            {
                if (interactive)
                {
                    Console.WriteLine(_pr._rm.GetString("DeactivatingUsers", CultureInfo.GetCultureInfo(_pr._cultureName)));
                    Console.ReadLine();
                }

                string _writerMsg = "";
                foreach (string clientId in clientIds)
                {
                    //QAB https://servicespubqab.pgilab.com/Rest/V1/Services/Client.svc
                    //PROD https://services.pgiconnect.com/Rest/V1/Services/Client.svc
                    string _provURL = "";
                    if (_env == "QAB")
                    {
                        _provURL = string.Format("https://servicespubqab.pgilab.com/Rest/V1/Services/Client.svc/Client/deactivate?clientId={0}&token={1}", clientId, _hmacToken.ToString());
                    }
                    else if (_env == "PROD")
                    {
                        _provURL = string.Format("https://services.pgiconnect.com/Rest/V1/Services/Client.svc/Client/deactivate?clientId={0}&token={1}", clientId, _hmacToken.ToString());
                    }

                    System.Net.Http.HttpClient _httpClient = new System.Net.Http.HttpClient();
                    HttpResponseMessage result = null;
                    try
                    {
                        result = _httpClient.DeleteAsync(_provURL).GetAwaiter().GetResult();
                    }
                    catch (AggregateException aex)
                    {
                        Console.WriteLine(aex.Message.ToString());
                    }
                    catch (System.Net.WebException e)
                    {
                        Console.WriteLine(e.Message.ToString());
                    }
                    catch (Newtonsoft.Json.JsonException e)
                    {
                        Console.WriteLine(e.Message.ToString());
                    }
                    string response = result.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    string _email = "";

                    _clients.TryGetValue(clientId, out _email);
                    _writerMsg += _email + " " + result.ReasonPhrase + System.Environment.NewLine;

                    List<string> _errors = new List<string>();

                    if (interactive)
                    {
                        foreach (var _error in ExtractJsonFieldErrors(response))
                        {
                            _errors.Add(_error);
                        }

                        if (_errors.Count() > 0)
                        {
                            Console.WriteLine(_pr._rm.GetString("ErrorInJSON", CultureInfo.GetCultureInfo(_pr._cultureName)));

                            foreach (var _error in _errors)
                            {
                                Console.WriteLine(_error);
                            }

                            Console.WriteLine(_pr._rm.GetString("PressAnyKeyExit", CultureInfo.GetCultureInfo(_pr._cultureName)));
                            Console.ReadLine();
                        }
                    }

                    continue;
                }

                using (StreamWriter _sw = new StreamWriter(_logfilePath + "Deactivations_" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                {
                    _sw.WriteLine(_writerMsg);
                }
            }
            catch (Exception ex)
            {
                using (StreamWriter _sw = new StreamWriter(_logfilePath + "ProvisionNonACP_Errors_" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                {
                    _sw.WriteLine(ex.ToString());
                }

            }
        }

        //Get PGi provisioned users by company Id
        private Dictionary<string,string> GetPGi()
        {
            //Program _pr = new Program();
            if(interactive)
            {
                Console.WriteLine(_rm.GetString("GettingClientIds", CultureInfo.GetCultureInfo(_cultureName)));
                Console.ReadLine();
            }
            Dictionary<string,string> _users = new Dictionary<string,string>();

            try
            {
                //QAB https://servicespubqab.pgilab.com/Rest/V1/Services/Client.svc
                //PROD https://services.pgiconnect.com/Rest/V1/Services/Client.svc

                string _provURL = "";

                if (_env == "QAB")
                {
                    _provURL = string.Format("https://servicespubqab.pgilab.com/Rest/V1/Services/Client.svc/Client/by?companyId={0}&token={1}", _companyId, _hmacToken.ToString());
                }
                else if (_env == "PROD")
                {
                    _provURL = string.Format("https://services.pgiconnect.com/Rest/V1/Services/Client.svc/Client/by?companyId={0}&token={1}", _companyId, _hmacToken.ToString());
                }

                System.Net.Http.HttpClient _httpClient = new System.Net.Http.HttpClient();
                HttpResponseMessage result = null;
                try
                {
                    result = _httpClient.GetAsync(_provURL).GetAwaiter().GetResult();
                }
                catch (AggregateException aex)
                {
                    Console.WriteLine(aex.Message.ToString());
                }
                catch (System.Net.WebException e)
                {
                    Console.WriteLine(e.Message.ToString());
                }
                catch (Newtonsoft.Json.JsonException e)
                {
                    Console.WriteLine(e.Message.ToString());
                }
                string response = result.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                if (_logfilePath.Replace(" ", "").Substring(_logfilePath.Length - 1) != "\\")
                {
                    _logfilePath += "\\";
                }
                using (StreamWriter _sw = new StreamWriter(_logfilePath + "PGi_Accounts" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                {
                    _sw.WriteLine("REST URL: " + _provURL.Replace(_companyId.ToString(), "").Replace(_hmacToken.ToString(), ""));
                    _sw.WriteLine("ReasonPhrase: " + result.ReasonPhrase);
                    _sw.WriteLine("Response: " + response);
                }


                List<string> _errors = new List<string>();

                foreach (var _error in ExtractJsonFieldErrors(response))
                {
                    _errors.Add(_error);
                }
                if (interactive)
                {
                    if (_errors.Count() > 0)
                    {
                        Console.WriteLine(_rm.GetString("ErrorInJSON", CultureInfo.GetCultureInfo(_cultureName)));

                        foreach (var _error in _errors)
                        {
                            Console.WriteLine(_error);
                        }

                        Console.WriteLine(_rm.GetString("PressAnyKeyExit", CultureInfo.GetCultureInfo(_cultureName)));
                        Console.ReadLine();
                        return null;
                    }
                }
                else
                {
                    if (_errors.Count() > 0)
                    {
                        return null;
                    }
                }
                _users = ExtractJsonFieldStringArray(response, "ByResult.SearchResults", "Contact.Email", "ClientId");

                if (interactive)
                {
                    Console.WriteLine(_rm.GetString("DiscoveredUsers", CultureInfo.GetCultureInfo(_cultureName)));
                    Console.ReadLine();
                }
            }
            catch(Exception ex)
            {
                using (StreamWriter _sw = new StreamWriter(_logfilePath + "ProvisionerError_" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                {
                    _sw.WriteLine(ex.ToString());
                }
            }

            return _users;
        }

        //Get O365 users with ACP info to determine set of users still provisioned on PGi not in O365
        private List<string> GetO365()
        {
            List<string> _users = new List<string>();
            List<string> _usersFinal = new List<string>();
            List<string> _enabledUsers = new List<string>();

            try
            {
                using (PowerShell ps = CreatePowershell())
                {
                    ps.Runspace.SessionStateProxy.SetVariable("myUsername", _username);
                    ps.Runspace.SessionStateProxy.SetVariable("myPassword", _pass);
                    ps.Commands.AddScript("Set-ExecutionPolicy Unrestricted -force");
                    ps.Commands.AddScript("enable-psremoting -force");
                    ps.Commands.AddScript("set-winrmnetworkdelayms 30000");
                    ps.Commands.AddScript("$myPassword = $myPassword | convertto-securestring");
                    ps.Commands.AddScript("$credentials = new-object -typename System.Management.Automation.PSCredential -argumentlist $myUsername,$myPassword");
                    ps.Commands.AddScript("Import-Module SkypeOnlineConnector");
                    ps.Commands.AddScript("$session = New-CsOnlineSession -Credential $credentials -Verbose");
                    ps.Commands.AddScript("Import-PSSession -Session $session");
                    ps.Commands.AddScript("Import-Module MsOnline");
                    ps.Commands.AddScript("Connect-MsolService -Credential $credentials");
                    ps.Commands.AddScript("$global:usersWithACP = Get-MsolUser -All -ReturnDeletedUsers " + _deleteFilter);
                    //ps.Commands.AddScript("$global:usersWithACP = Get-CsOnlineUser " + _filter.Replace("AcpInfo -eq", "AcpInfo -ne"));
                    //ps.Commands.AddScript("ForEach ($userWithACP in $usersWithACP) {echo $userWithACP.SipAddress}");

                    foreach (var _psItem in ps.Invoke())
                    {
                        if (_psItem != null && !_psItem.ToString().Contains("pgibot"))
                        {
                            _users.Add(_psItem.ToString().ToLower().Replace("sip:", ""));
                        }
                    }

                    ps.Commands.AddScript("Remove-PSSession -Session $session");
                    ps.Invoke();
                    ps.Runspace.Close();
                    ps.Runspace.Dispose();
                }

                GetDisabledO365Users();

                if(_disabledUsers.Count > 0)
                {
                    foreach(string _accountDisabled in _disabledUsers)
                    {
                        if(_users.Contains(_accountDisabled.Replace("sip:","")))
                        {
                            _usersFinal.Add(_accountDisabled.Replace("sip:", ""));
                        }
                    }
                }
                else
                {
                    _users = null;
                    _usersFinal = null;
                }
            }
            catch(Exception ex)
            {
                using (StreamWriter _sw = new StreamWriter(_logfilePath + "ProvisionerError_" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                {
                    _sw.WriteLine(ex.ToString());
                }
            }
            return _usersFinal;
        }

        private string GetCompanyPGiOptions()
        {
            string _options = "";
            string _provURL = "";

            if (_env == "QAB")
            {
                _provURL = string.Format("https://servicespubqab.pgilab.com/Rest/V1/Services/Company.svc/Company/{0}?token={1}", _companyId, _hmacToken.ToString());
            }
            else if (_env == "PROD")
            {
                _provURL = string.Format("https://services.pgiconnect.com/Rest/V1/Services/Company.svc/Company/{0}?token={1}", _companyId, _hmacToken.ToString());
            }

            System.Net.Http.HttpClient _httpClient = new System.Net.Http.HttpClient();
            HttpResponseMessage result = null;
            try
            {
                result = _httpClient.GetAsync(_provURL).GetAwaiter().GetResult();
            }
            catch (AggregateException aex)
            {
                Console.WriteLine(aex.Message.ToString());
            }
            catch (System.Net.WebException e)
            {
                Console.WriteLine(e.Message.ToString());
            }
            catch (Newtonsoft.Json.JsonException e)
            {
                Console.WriteLine(e.Message.ToString());
            }
            string response = result.Content.ReadAsStringAsync().GetAwaiter().GetResult();

            List<string> _errors = new List<string>();

            foreach (var _error in ExtractJsonFieldErrors(response))
            {
                _errors.Add(_error);
            }

            if (_errors.Count() > 0)
            {
                Program _pr = new Program();
                Console.WriteLine(_pr._rm.GetString("ErrorInJSON", CultureInfo.GetCultureInfo(_pr._cultureName)));

                foreach (var _error in _errors)
                {
                    Console.WriteLine(_error);
                }

                Console.WriteLine(_pr._rm.GetString("PressAnyKeyExit", CultureInfo.GetCultureInfo(_pr._cultureName)));
                Console.ReadLine();
                return null;
            }

            var _tempOptions = ExtractJsonField(response, "CompanyGetResult.GetResults", false);
            _defaultLanguage = ExtractJsonField(_tempOptions, "Company.DefaultLanguage");
            _options = ExtractJsonField(_tempOptions, "Options.DefaultConferenceOptionCodes");
            
            if(!string.IsNullOrWhiteSpace(_options))
            {
                if(_options.Contains("1024.2"))
                {
                    _passcodeReqs = "Enhanced Authentication for GM Selected";
                }
            }

            return _options;
        }

        //Provision O365 users in PGi
        private ACPUser ProvisionPGi(string user)
        {
            ACPUser _user = new ACPUser();
            try
            {
                if (String.IsNullOrWhiteSpace(user) && user.Split(',')[0].Contains("pgibot"))
                {
                    return null;
                }

                int CompanyId = _companyId;
                string SipAddress = user.Split(',')[0].ToLower();
                ProvisionUser.Address _provUserAddress = new ProvisionUser.Address();
                ProvisionUser.AdHoc _provUserAdhoc = new ProvisionUser.AdHoc();
                ProvisionUser.Billing _provUserBilling = new ProvisionUser.Billing();
                ProvisionUser.Client _provUserClient = new ProvisionUser.Client();
                ProvisionUser.Comments _provUserComments = new ProvisionUser.Comments();
                ProvisionUser.ConnectMeNumber _provUserConnectMeNumber = new ProvisionUser.ConnectMeNumber();
                ProvisionUser.Contact _provUserContact = new ProvisionUser.Contact();
                ProvisionUser.CustomData _provUserCustomData = new ProvisionUser.CustomData();
                ProvisionUser.Daily _provUserDaily = new ProvisionUser.Daily();
                ProvisionUser.Monthly _provUserMonthly = new ProvisionUser.Monthly();
                ProvisionUser.MonthlyDate _provUserMonthlyDate = new ProvisionUser.MonthlyDate();
                ProvisionUser.PassCodeRequirements _provUserPassCodeRequirements = new ProvisionUser.PassCodeRequirements();
                ProvisionUser.Reservation _provUserReservation = new ProvisionUser.Reservation();
                ProvisionUser.ReservationCreateMessage _provUserReservationCreateMessage = new ProvisionUser.ReservationCreateMessage();
                ProvisionUser.SaiClientCreateMessage _provUserSaiClientCreateMessage = new ProvisionUser.SaiClientCreateMessage();
                ProvisionUser.Schedule _provUserSchedule = new ProvisionUser.Schedule();
                ProvisionUser.Skip _provUserSkip = new ProvisionUser.Skip();
                ProvisionUser.Weekday _provUserWeekday = new ProvisionUser.Weekday();
                ProvisionUser.Weekly _provUserWeekly = new ProvisionUser.Weekly();


                _provUserClient.FirstName = user.Split(',')[1];
                _provUserClient.LastName = user.Split(',')[2];
                _provUserClient.Password = "";
                _provUserClient.DefaultLanguage = "en";
                _provUserClient.SetAsCompanyDefaultBillTo = false;
                _provUserClient.AlwaysUseSoftphone = false;
                _provUserBilling.POCC = user.Split(',')[5];
                _provUserBilling.SpecialInfo = user.Split(',')[5];
                _provUserReservation.POCC = user.Split(',')[5];
                _provUserContact.Email = user.Split(',')[0].ToLower().Replace("sip:", "");
                _provUserReservationCreateMessage.Reservation = _provUserReservation;
                _provUserSaiClientCreateMessage.ReservationCreateMessage = _provUserReservationCreateMessage;
                _provUserSaiClientCreateMessage.Client = _provUserClient;
                _provUserSaiClientCreateMessage.CompanyId = CompanyId;
                _provUserSaiClientCreateMessage.Contact = _provUserContact;
                _provUserSaiClientCreateMessage.SipAddress = SipAddress;

                string _json = CreateJsonClientString(_provUserSaiClientCreateMessage, true);
                //QAB https://servicespubqab.pgilab.com/Rest/V1/Services/Client.svc
                //PROD https://services.pgiconnect.com/Rest/V1/Services/Client.svc
                //string _provURL = 
                string _provURL = "";

                if (_env == "QAB")
                {
                    _provURL = string.Format("https://servicespubqab.pgilab.com/Rest/V1/Services/Client.svc/Client/Create?token={0}", _hmacToken.ToString());
                }
                else if (_env == "PROD")
                {
                    _provURL = string.Format("https://services.pgiconnect.com/Rest/V1/Services/Client.svc/Client/Create?token={0}", _hmacToken.ToString());
                }


                var content = new StringContent(_json, Encoding.UTF8, "application/json");

                System.Net.Http.HttpClient _httpClient = new System.Net.Http.HttpClient();
                HttpResponseMessage result = null;
                try
                {
                    result = _httpClient.PostAsync(_provURL, content).GetAwaiter().GetResult();
                }
                catch (AggregateException aex)
                {
                    Console.WriteLine(aex.Message.ToString());
                }
                catch (System.Net.WebException e)
                {
                    Console.WriteLine(e.Message.ToString());
                }
                catch (Newtonsoft.Json.JsonException e)
                {
                    Console.WriteLine(e.Message.ToString());
                }
                string response = result.Content.ReadAsStringAsync().GetAwaiter().GetResult();

                _provisionResponse += user.Split(',')[1] + " " + user.Split(',')[2] + " " + user.Split(',')[0].ToLower().Replace("sip:", "") + " " + result.ReasonPhrase + System.Environment.NewLine;

                List<string> _errors = new List<string>();

                foreach (var _error in ExtractJsonFieldErrors(response))
                {
                    _errors.Add(_error);
                }

                if (_errors.Count() > 0)
                {
                    if (interactive)
                    {
                        Program _pr = new Program();
                        Console.WriteLine(_pr._rm.GetString("ErrorInJSON", CultureInfo.GetCultureInfo(_pr._cultureName)));

                        foreach (var _error in _errors)
                        {
                            Console.WriteLine(_error);
                        }

                        Console.WriteLine(_pr._rm.GetString("PressAnyKeyExit", CultureInfo.GetCultureInfo(_pr._cultureName)));
                        Console.ReadLine();
                        return null;
                    }
                    else
                    {
                        string _errString = "";
                        foreach (var error in _errors)
                        {
                            _errString += error.ToString() + System.Environment.NewLine;
                        }
                        using (StreamWriter _sw = new StreamWriter(_logfilePath + "ProvisionNonACP_Errors_" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                        {
                            _sw.WriteLine(_errString);
                        }

                        return null;
                    }
                }


                string _clientId = ExtractJsonField(response, "ClientCreateResult.ClientId");
                string _passCode = ExtractJsonField(response, "ClientCreateResult.Reservation.PassCodes.ParticipantPassCode");

                if(_passCode == null)
                {
                    return null;
                }

                string _hubURL = ExtractJsonField(response, "ClientCreateResult.ClientHierarchy.HubUrl");
                List<string> _tollNum = ExtractJsonFieldTollNumber(response);
                Dictionary<string,string> _tollFreeNum = ExtractJsonFieldTollFreeNumber(response);
                
                _user.TollNumbers = new List<string>();
                _user.TollFreeNumbers = new List<string>();

                _user.Name = _provUserClient.FirstName.Replace(System.Environment.NewLine, "").Replace(" ", "") + " " + _provUserClient.LastName.Replace(System.Environment.NewLine, "").Replace(" ", "");
                _user.Email = _provUserContact.Email.Replace(System.Environment.NewLine, "").Replace(" ", "");
                //_user.Domain = _hubURL.Split('.')[1] + "." + _hubURL.Split('.')[2];
                _user.Domain = _hubURL.Replace(System.Environment.NewLine, "").Replace(" ", "");
                _user.ParticipantPasscode = _passCode != null ? _passCode.Replace(System.Environment.NewLine, "").Replace(" ", "") : "";

                //if (_tollFreeNum != null)
                //{
                //    //O365 CountryOrRegionDisplayName and City need to be filled.
                //    List<string> _tempTollFree = new List<string>();                    

                //    foreach (var elem in _tollFreeNum)
                //    {
                //        string CountryOrRegionDisplayName = user.Split(',')[3];
                //        string City = user.Split(',')[6];

                //        while (_tempTollFree.Count() < 5)
                //        {
                //            if (!string.IsNullOrWhiteSpace(elem.Value))
                //            {
                //                if (elem.Value == CountryOrRegionDisplayName + ", " + City)
		              //          {
                //                    _tempTollFree.Add(elem.Key);
                //                    //_tollFreeNum.Remove(elem.Key);
                //                }

                //            }
                //            else if (_tempTollFree.Count() < 5)
                //            {
                //                int _diff = 5 - _tempTollFree.Count();
                //                _tempTollFree.AddRange(_tollFreeNum.Keys.Where(w => !_tempTollFree.Contains(w)).Take(_diff));
                //            }
                //        }                        
                //    }

                //    _user.TollFreeNumbers = _tempTollFree;

                //    foreach(string num in _user.TollFreeNumbers)
                //    {
                //        num.Replace(System.Environment.NewLine, "").Replace(" ", "");
                //    }
                //}

                if (_tollFreeNum != null)
                {
                    foreach (var num in _tollFreeNum)
                    {
                        _user.TollFreeNumbers.Add(num.Key.Replace(System.Environment.NewLine, "").Replace(" ", ""));
                    }
                }

                if (_tollNum != null)
                {
                    foreach (var elem in _tollNum)
                    {
                        _user.TollNumbers.Add(elem.Replace(System.Environment.NewLine, "").Replace(" ", ""));
                    }
                }

                _user.URL = _hubURL.Replace(System.Environment.NewLine, "").Replace(" ", "");
                //}
            }
            catch(Exception ex)
            {
                _user = null;
                using (StreamWriter _sw = new StreamWriter(_logfilePath + "ProvisionNonACP_Errors_" + DateTime.Now.ToString("MM-dd-yyyy_hh.mm.ss.tt") + ".txt"))
                {
                    _sw.WriteLine(ex.ToString());
                }

            }

            return _user;
        }

        //Creates JSON string for provisioning in PGi
        private string CreateJsonClientString(ProvisionUser.SaiClientCreateMessage message, bool minMsg = true)
        {
            string _jsonString = "";
            if(string.IsNullOrWhiteSpace(_defaultLanguage))
            {
                _defaultLanguage = "en";
            }

            _jsonString += "{";
            _jsonString += "\"clientCreateMessage\": {";
            _jsonString += "\"CompanyId\": " + message.CompanyId + ",";
            _jsonString += "\"Client\": {";
            _jsonString += "\"FirstName\": " + "\"" + message.Client.FirstName + "\",";
            _jsonString += "\"LastName\": " + "\"" + message.Client.LastName + "\",";
            _jsonString += "\"Password\": "+ "\"" + message.Client.Password + "\",";
            _jsonString += "\"DefaultLanguage\": "+ "\"" + _defaultLanguage + "\",";
            _jsonString += "\"SetAsCompanyDefaultBillTo\": " + message.Client.SetAsCompanyDefaultBillTo.ToString().ToLower() + ",";
            _jsonString += "\"AlwaysUseSoftphone\": " + message.Client.AlwaysUseSoftphone.ToString().ToLower();
            _jsonString += "},";                
            _jsonString += "\"Contact\": {";
            _jsonString += "\"Email\": " + "\"" + message.Contact.Email + "\"";                
            _jsonString += "},";
            if (_splitBilling)
            {
                _jsonString += "\"Billing\": {";
                _jsonString += "\"POCC\": " + "\"" + message.ReservationCreateMessage.Reservation.POCC + "\"";
                //_jsonString += "\"POCC\": " + "\"" + message.ReservationCreateMessage.Reservation.POCC + "\",";
                //_jsonString += "\"SpecialInfo\": " + "\"" + message.ReservationCreateMessage.Reservation.POCC + "\"";
                _jsonString += "},";
            }
            _jsonString += "\"ReservationCreateMessage\": {";
            _jsonString += "\"SendEmail\": true,";
            _jsonString += "\"Reservation\": {";
            //if (_splitBilling)
            //{
            //    _jsonString += "\"POCC\": \"" + message.ReservationCreateMessage.Reservation.POCC + "\",";
            //}
            _jsonString += "\"ModeratorName\": \"" + message.Client.FirstName + " " + message.Client.LastName + "\",";
            _jsonString += "\"Email\": \"" + message.Contact.Email + "\",";
            _jsonString += "\"ConferenceName\": \"Initial Conference\",";
            _jsonString += "\"ConferenceType\": \"" + message.ReservationCreateMessage.Reservation.ConferenceType + "\"";
            _jsonString += "},";
            _jsonString += "\"BridgeOptionCodes\": [\"GlobalMeet2\"],";

            //if (_passcodeReqs == "EnhancedAuthenticationGlobalMeetDisabled")
            //{
            //    _jsonString += "\"ConferenceOptionCodes\": [\"MusicOnHold\"],";
            //}
            //else
            //{
            //    _jsonString += "\"ConferenceOptionCodes\": [\"MusicOnHold\", \"UseSecurityCode\"],";
            //}
            
            _jsonString += "\"PassCodeRequirements\": {";            
            _jsonString += "\"PassCodeType\": \"Random10DigitStrong\"";
            _jsonString += "},";
            _jsonString += "\"UseDefaultOptions\": \"true\"";
            _jsonString += "}";            
            _jsonString += "}";
            _jsonString += "}";
            
            return _jsonString;
        }
    }
}