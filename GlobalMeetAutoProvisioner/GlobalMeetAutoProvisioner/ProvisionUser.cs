﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalMeetAutoProvisioner
{
    class ProvisionUser
    {
        public class Comments
        {
            public string CommentType { get; set; } = "";
            public string Comment { get; set; } = "";
        }

        public class Client
        {
            public string FirstName { get; set; } = "";
            public string LastName { get; set; } = "";
            public string Password { get; set; } = "";
            public int HubId { get; set; } = 0;
            public bool Assistant { get; set; } = true;
            public DateTime ExpirationDate { get; set; }
            public string ExternalId { get; set; } = "";
            public string OperatorComments { get; set; } = "";
            public string TimeZoneCode { get; set; } = "";
            public int RoleCode { get; set; } = 0;
            public bool DoNotSolicit { get; set; } = false;
            public string DefaultLanguage { get; set; } = "";
            public bool SetAsCompanyDefaultBillTo { get; set; } = true;
            public bool AlwaysUseSoftphone { get; set; } = true;
            public string CustomAuthentication { get; set; } = "";
            public List<Comments> Comments { get; set; } = new List<Comments>();
        }

        public class Address
        {
            public string Address1 { get; set; } = "";
            public string Address2 { get; set; } = "";
            public string Address3 { get; set; } = "";
            public string City { get; set; } = "";
            public string StateCode { get; set; } = "";
            public string PostalCode { get; set; } = "";
            public string CountryCode { get; set; } = "";
            public string Province { get; set; } = "";
        }

        public class Contact
        {
            public string JobTitle { get; set; } = "";
            public string Phone { get; set; } = "";
            public string PhoneExt { get; set; } = "";
            public string PhoneIntlPrefix { get; set; } = "";
            public bool PhoneIsDefault { get; set; } = true;
            public string Fax { get; set; } = "";
            public string FaxIntlPrefix { get; set; } = "";
            public string MobilePhone { get; set; } = "";
            public string MobilePhoneIntlPrefix { get; set; } = "";
            public string MobilePhoneExt { get; set; } = "";
            public bool MobilePhoneIsDefault { get; set; } = true;
            public string HomePhone { get; set; } = "";
            public string HomePhoneIntlPrefix { get; set; } = "";
            public string HomePhoneExt { get; set; } = "";
            public bool HomePhoneIsDefault { get; set; } = true;
            public string SecondaryPhone { get; set; } = "";
            public string SecondaryPhoneIntlPrefix { get; set; } = "";
            public string SecondaryPhoneExt { get; set; } = "";
            public bool SecondaryPhoneIsDefault { get; set; } = true;
            public string Email { get; set; } = "";
            public Address Address { get; set; } = new Address();
        }

        public class ConnectMeNumber
        {
            public string Extension { get; set; } = "";
            public string IntlPrefix { get; set; } = "";
            public string PhoneNumber { get; set; } = "";
            public string PhoneTypeDescription { get; set; } = "";
        }

        public class Billing
        {
            public string POCC { get; set; } = "";
            public string HowHeardCode { get; set; } = "";
            public string SpecialInfo { get; set; } = "";
        }

        public class CustomData
        {
            public string CustomFieldName { get; set; } = "";
            public string CustomFieldCode { get; set; } = "";
            public string CustomFieldType { get; set; } = "";
            public string CustomValue { get; set; } = "";
            public List<string> ListOptions { get; set; } = new List<string>();
            public string Masking { get; set; } = "";
            public string Hint { get; set; } = "";
        }

        public class Reservation
        {
            public string POCC { get; set; } = "";
            public string ModeratorName { get; set; } = "";
            public string Email { get; set; } = "";
            public string ConferenceName { get; set; } = "";
            public DateTime DeletedDate { get; set; }
            public int ClientNumberMinLength { get; set; } = 0;
            public int ClientNumberMaxLength { get; set; } = 0;
            public int MatterNumberMinLength { get; set; } = 0;
            public int MatterNumberMaxLength { get; set; } = 0;
            public bool IsClientDefault { get; set; } = false;
            public string ConferenceType { get; set; } = "SkypeOnline";
        }

        public class PassCodeRequirements
        {
            public bool SecurePassCodes { get; set; } = true;
            public string ParticipantPassCode { get; set; } = "";
            public string ModeratorPassCode { get; set; } = "";
            public string SecurityCode { get; set; } = "";
            public string PassCodeType { get; set; } = "Random10DigitStrong";
        }

        public class AdHoc
        {
            public DateTime StartDateTime { get; set; }
            public int Duration { get; set; } = 0;
        }

        public class Daily
        {
            public DateTime StartDateTime { get; set; }
            public DateTime EndDate { get; set; }
            public int Duration { get; set; } = 0;
            public int Rate { get; set; } = 0;
        }

        public class Monthly
        {
            public DateTime StartDateTime { get; set; }
            public DateTime EndDate { get; set; }
            public int Duration { get; set; } = 0;
            public int Rate { get; set; } = 0;
            public int Occurrence { get; set; } = 0;
            public string Day { get; set; } = "";
        }

        public class MonthlyDate
        {
            public DateTime StartDateTime { get; set; }
            public DateTime EndDate { get; set; }
            public int Duration { get; set; } = 0;
            public int Rate { get; set; } = 0;
            public int DayNumber { get; set; } = 0;
        }

        public class Skip
        {
            public DateTime StartDateTime { get; set; }
        }

        public class Weekday
        {
            public DateTime StartDateTime { get; set; }
            public DateTime EndDate { get; set; }
            public int Duration { get; set; } = 0;
        }

        public class Weekly
        {
            public DateTime StartDateTime { get; set; }
            public DateTime EndDate { get; set; }
            public int Duration { get; set; } = 0;
            public int Rate { get; set; } = 0;
            public string Day { get; set; } = "";
        }

        public class Schedule
        {
            public List<AdHoc> AdHoc { get; set; } = new List<AdHoc>();
            public List<Daily> Daily { get; set; } = new List<Daily>();
            public List<Monthly> Monthly { get; set; } = new List<Monthly>();
            public List<MonthlyDate> MonthlyDate { get; set; } = new List<MonthlyDate>();
            public List<Skip> Skip { get; set; } = new List<Skip>();
            public List<Weekday> Weekdays { get; set; } = new List<Weekday>();
            public List<Weekly> Weekly { get; set; } = new List<Weekly>();
            public string TimeZoneCode { get; set; } = "";
        }

        public class ReservationCreateMessage
        {
            public string SendEmail { get; set; } = "";
            public Reservation Reservation { get; set; } = new Reservation();
            public List<string> BridgeOptionCodes { get; set; } = new List<string>();
            public List<string> ConferenceOptionCodes { get; set; } = new List<string>();
            public PassCodeRequirements PassCodeRequirements { get; set; } = new PassCodeRequirements();
            public Schedule Schedule { get; set; } = new Schedule();
        }

        public class SaiClientCreateMessage
        {
            public string SipAddress { get; set; } = "";
            public int SkypeId { get; set; } = 0;
            public int CompanyId { get; set; } = 0;
            public Client Client { get; set; } = new Client();
            public Contact Contact { get; set; } = new Contact();
            public List<ConnectMeNumber> ConnectMeNumbers { get; set; } = new List<ConnectMeNumber>();
            public Billing Billing { get; set; } = new Billing();
            public List<CustomData> CustomData { get; set; } = new List<CustomData>();
            public ReservationCreateMessage ReservationCreateMessage { get; set; } = new ReservationCreateMessage();
        }

        public class RootObject
        {
            public SaiClientCreateMessage saiClientCreateMessage { get; set; } = new SaiClientCreateMessage();
        }

    }
}
