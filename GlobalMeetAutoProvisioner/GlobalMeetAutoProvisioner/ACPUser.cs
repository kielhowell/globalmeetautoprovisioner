﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalMeetAutoProvisioner
{
    class ACPUser
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public List<string> TollNumbers { get; set; }
        public List<string> TollFreeNumbers { get; set; }
        public string ParticipantPasscode { get; set; }
        public string Domain { get; set; }
        public string URL { get; set; }
    }
}
