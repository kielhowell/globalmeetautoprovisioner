﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalMeetAutoProvisioner
{
    class CultureValues
    {
        public const String US = "en-US";
        public const String Japanese = "ja-JP";
        public const String German = "de-DE";
        public const String French = "fr-FR";
    }
}
